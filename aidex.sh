##!/bin/sh
## ./ry.sh start 启动 stop 停止 restart 重启 status 状态
#AppName=ruoyi-admin.jar
#
## JVM参数
#JVM_OPTS="-Dname=$AppName  -Duser.timezone=Asia/Shanghai -Xms512m -Xmx1024m -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=512m -XX:+HeapDumpOnOutOfMemoryError -XX:+PrintGCDateStamps  -XX:+PrintGCDetails -XX:NewRatio=1 -XX:SurvivorRatio=30 -XX:+UseParallelGC -XX:+UseParallelOldGC"
#APP_HOME=`pwd`
#LOG_PATH=$APP_HOME/logs/$AppName.log
#
#if [ "$1" = "" ];
#then
#    echo -e "\033[0;31m 未输入操作名 \033[0m  \033[0;34m {start|stop|restart|status} \033[0m"
#    exit 1
#fi
#
#if [ "$AppName" = "" ];
#then
#    echo -e "\033[0;31m 未输入应用名 \033[0m"
#    exit 1
#fi
#
#function start()
#{
#    PID=`ps -ef |grep java|grep $AppName|grep -v grep|awk '{print $2}'`
#
#	if [ x"$PID" != x"" ]; then
#	    echo "$AppName is running..."
#	else
#		nohup java $JVM_OPTS -jar $AppName > /dev/null 2>&1 &
#		echo "Start $AppName success..."
#	fi
#}
#
#function stop()
#{
#    echo "Stop $AppName"
#
#	PID=""
#	query(){
#		PID=`ps -ef |grep java|grep $AppName|grep -v grep|awk '{print $2}'`
#	}
#
#	query
#	if [ x"$PID" != x"" ]; then
#		kill -TERM $PID
#		echo "$AppName (pid:$PID) exiting..."
#		while [ x"$PID" != x"" ]
#		do
#			sleep 1
#			query
#		done
#		echo "$AppName exited."
#	else
#		echo "$AppName already stopped."
#	fi
#}
#
#function restart()
#{
#    stop
#    sleep 2
#    start
#}
#
#function status()
#{
#    PID=`ps -ef |grep java|grep $AppName|grep -v grep|wc -l`
#    if [ $PID != 0 ];then
#        echo "$AppName is running..."
#    else
#        echo "$AppName is not running..."
#    fi
#}
#
#case $1 in
#    start)
#    start;;
#    stop)
#    stop;;
#    restart)
#    restart;;
#    status)
#    status;;
#    *)
#
#esac


#!/bin/bash

#修改APP_NAME为云效上的应用名
APP_NAME=aidex-admin


PROG_NAME=$0
ACTION=$1
APP_START_TIMEOUT=20    # 等待应用启动的时间
APP_PORT=8080          # 应用端口
HEALTH_CHECK_URL=http://127.0.0.1:${APP_PORT}/first # 应用健康检查URL
HEALTH_CHECK_FILE_DIR=/home/admin/status   # 脚本会在这个目录下生成nginx-status文件
APP_HOME=/home/admin/aidex-admin/ # 从package.tgz中解压出来的jar包放到这个目录下
JAR_NAME=/home/admin/app/aidex-admin/aidex-admin/target/${APP_NAME}.jar # jar包的名字
JAVA_OUT=${APP_HOME}/logs/start.log  #应用的启动日志

echo "进来了"
#创建出相关目录
mkdir -p ${HEALTH_CHECK_FILE_DIR}
mkdir -p ${APP_HOME}/logs
usage() {
    echo "Usage: $PROG_NAME {start|stop|restart}"
    exit 2
}

health_check() {
    exptime=0
    echo "checking ${HEALTH_CHECK_URL}"
    while true
        do
            status_code=`/usr/bin/curl -L -o /dev/null --connect-timeout 5 -s -w %{http_code}  ${HEALTH_CHECK_URL}`
            if [ "$?" != "0" ]; then
               echo -n -e "\rapplication not started"
            else
                echo "code is $status_code"
                if [ "$status_code" == "200" ];then
                    break
                fi
            fi
            sleep 1
            ((exptime++))

            echo -e "\rWait app to pass health check: $exptime..."

            if [ $exptime -gt ${APP_START_TIMEOUT} ]; then
                echo 'app start failed'
               exit 1
            fi
        done
    echo "check ${HEALTH_CHECK_URL} success"
}
start_application() {
    echo "starting java process"
    nohup java -jar ${JAR_NAME} > ${JAVA_OUT} 2>&1 &
    echo "started java process"
}

stop_application() {
   checkjavapid=`ps -ef | grep java | grep ${APP_NAME} | grep -v grep |grep -v 'deploy.sh'| awk '{print$2}'`

   if [[ ! $checkjavapid ]];then
      echo -e "\rno java process"
      return
   fi

   echo "stop java process"
   times=60
   for e in $(seq 60)
   do
        sleep 1
        COSTTIME=$(($times - $e ))
        checkjavapid=`ps -ef | grep java | grep ${APP_NAME} | grep -v grep |grep -v 'deploy.sh'| awk '{print$2}'`
        if [[ $checkjavapid ]];then
            kill -9 $checkjavapid
            echo -e  "\r        -- stopping java lasts `expr $COSTTIME` seconds."
        else
            echo -e "\rjava process has exited"
            break;
        fi
   done
   echo ""
}
start() {
    start_application
    health_check
}
stop() {
    stop_application
}
case "$ACTION" in
    start)
        start
    ;;
    stop)
        stop
    ;;
    restart)
        stop
        start
    ;;
    *)
        usage
    ;;
esac
