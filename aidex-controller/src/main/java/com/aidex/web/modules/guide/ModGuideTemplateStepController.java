package com.aidex.web.modules.guide;

import java.util.List;
import java.util.HashMap;
import java.util.Map;
import com.aidex.common.annotation.Log;
import com.aidex.common.core.domain.R;
import com.github.pagehelper.PageInfo;
import com.aidex.common.core.page.PageDomain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.aidex.common.enums.BusinessType;
import com.aidex.common.utils.poi.ExcelUtil;
import com.aidex.framework.cache.DictUtils;
import javax.validation.constraints.*;
import org.springframework.web.bind.annotation.*;
import com.aidex.common.core.controller.BaseController;
import org.springframework.validation.annotation.Validated;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import com.aidex.modules.domain.ModGuideTemplateStep;
import com.aidex.modules.service.ModGuideTemplateStepService;

/**
 * 引导模板步骤Controller
 * @author anewhen
 * @email anewhen@foxmail.com
 * @date 2022-05-17
 */
@RestController
@RequestMapping("/mod/guide/template/step")
public class ModGuideTemplateStepController extends BaseController {

    @Autowired
    private ModGuideTemplateStepService modGuideTemplateStepService;

    /**
     * 查询引导模板步骤列表
     */
    @PreAuthorize("@ss.hasPermi('modules:GuideTemplateStep:list')")
    @GetMapping("/list")
    public R<PageInfo> list(ModGuideTemplateStep modGuideTemplateStep, HttpServletRequest request, HttpServletResponse response) {
        modGuideTemplateStep.setPage(new PageDomain(request, response));
        return R.data(modGuideTemplateStepService.findPage(modGuideTemplateStep));
    }

    /**
     * 根据模板id获取步骤清单
     * @param id
     * @return
     */
    @PreAuthorize("@ss.hasPermi('modules:GuideTemplateStep:list')")
    @GetMapping("/listByTemplateId")
    public R<List<ModGuideTemplateStep>> listByTemplateId(String id){
        return R.data(modGuideTemplateStepService.queryStepsByTemplateId(id));
    }

    /**
     * 获取引导模板步骤详细信息
     */
    @PreAuthorize("@ss.hasPermi('modules:GuideTemplateStep:query')")
    @GetMapping(value = "/{setpName}")
    public R<ModGuideTemplateStep> detail(@PathVariable("setpName") String setpName) {
        return R.data(modGuideTemplateStepService.get(setpName));
    }

    /**
     * 新增引导模板步骤
     */
    @PreAuthorize("@ss.hasPermi('modules:GuideTemplateStep:add')")
    @Log(title = "引导模板步骤", businessType = BusinessType.INSERT)
    @PostMapping
    public R add(@RequestBody @Validated  ModGuideTemplateStep modGuideTemplateStep) {
        return R.status(modGuideTemplateStepService.save(modGuideTemplateStep));
    }

    /**
     * 修改引导模板步骤
     */
    @PreAuthorize("@ss.hasPermi('modules:GuideTemplateStep:edit')")
    @Log(title = "引导模板步骤", businessType = BusinessType.UPDATE)
    @PutMapping
    public R edit(@RequestBody @Validated ModGuideTemplateStep modGuideTemplateStep) {
        return R.status(modGuideTemplateStepService.save(modGuideTemplateStep));
    }

    /**
     * 删除引导模板步骤
     */
    @PreAuthorize("@ss.hasPermi('modules:GuideTemplateStep:remove')")
    @Log(title = "引导模板步骤", businessType = BusinessType.DELETE)
    @DeleteMapping("/{setpNames}")
    public R remove(@PathVariable String[] ids) {
        return R.status(modGuideTemplateStepService.deleteModGuideTemplateStepByIds(ids));
    }


    /**
     * 导出引导模板步骤列表
     */
    @PreAuthorize("@ss.hasPermi('modules:GuideTemplateStep:export')")
    @Log(title = "引导模板步骤", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public R export(ModGuideTemplateStep modGuideTemplateStep) {
        List<ModGuideTemplateStep> list = modGuideTemplateStepService.findList(modGuideTemplateStep);
        ExcelUtil<ModGuideTemplateStep> util = new ExcelUtil<ModGuideTemplateStep>(ModGuideTemplateStep.class);
        return util.exportExcel(list, "引导模板步骤数据");
    }
	
    /**
     * 根据字典类型查询字典数据信息等其他自定义信息
     */
    @GetMapping(value = "/getInitData/{dictTypes}")
    public R getInitData(@PathVariable String dictTypes) {
        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.putAll(DictUtils.getMultiDictList(dictTypes));
        return R.data(dataMap);
    }

}
