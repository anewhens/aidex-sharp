package com.aidex.web.modules.guide;

import java.util.List;
import java.util.HashMap;
import java.util.Map;
import com.aidex.common.annotation.Log;
import com.aidex.common.core.domain.R;
import com.aidex.common.utils.StringUtils;
import com.aidex.modules.domain.ModGuideTemplate;
import com.aidex.modules.service.ModGuideTemplateService;
import com.aidex.modules.service.ModGuideTemplateStepService;
import com.github.pagehelper.PageInfo;
import com.aidex.common.core.page.PageDomain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.aidex.common.enums.BusinessType;
import com.aidex.common.utils.poi.ExcelUtil;
import com.aidex.framework.cache.DictUtils;
import javax.validation.constraints.*;
import org.springframework.web.bind.annotation.*;
import com.aidex.common.core.controller.BaseController;
import org.springframework.validation.annotation.Validated;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 引导模板管理Controller
 * @author anewhen@foxmail.com
 * @email anewhen@foxmail.com
 * @date 2022-05-16
 */
@RestController
@RequestMapping("/mod/guide/template")
public class ModGuideTemplateController extends BaseController {

    @Autowired
    private ModGuideTemplateService modGuideTemplateService;

    @Autowired
    private ModGuideTemplateStepService modGuideTemplateStepService;

    /**
     * 查询引导模板管理列表
     */
    @PreAuthorize("@ss.hasPermi('mod:GuideTemplate:list')")
    @GetMapping("/list")
    public R<PageInfo> list(ModGuideTemplate modGuideTemplate, HttpServletRequest request, HttpServletResponse response) {
        modGuideTemplate.setPage(new PageDomain(request, response));
        return R.data(modGuideTemplateService.findPage(modGuideTemplate));
    }

    /**
     * 获取引导模板管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('mod:GuideTemplate:query')")
    @GetMapping(value = "/{id}")
    public R<ModGuideTemplate> detail(@PathVariable("id") String id) {
        return R.data(modGuideTemplateService.get(id));
    }

    /**
     * 新增引导模板管理
     */
    @PreAuthorize("@ss.hasPermi('mod:GuideTemplate:add')")
    @Log(title = "引导模板管理", businessType = BusinessType.INSERT)
    @PostMapping
    public R add(@RequestBody @Validated  ModGuideTemplate modGuideTemplate) {
        return R.status(modGuideTemplateService.save(modGuideTemplate));
    }

    /**
     * 修改引导模板管理
     */
    @PreAuthorize("@ss.hasPermi('mod:GuideTemplate:edit')")
    @Log(title = "引导模板管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public R edit(@RequestBody @Validated ModGuideTemplate modGuideTemplate) {
        return R.status(modGuideTemplateService.save(modGuideTemplate));
    }

    /**
     * 更新引导模板管理状态
     */
    @PreAuthorize("@ss.hasPermi('mod:GuideTemplate:edit')")
    @Log(title = "引导模板管理", businessType = BusinessType.UPDATE)
    @PutMapping("/updateStatus")
    public R updateStatus(@RequestBody ModGuideTemplate modGuideTemplate) {
        return R.status(modGuideTemplateService.updateStatus(modGuideTemplate));
    }

    /**
     * 删除引导模板管理
     */
    @PreAuthorize("@ss.hasPermi('mod:GuideTemplate:remove')")
    @Log(title = "引导模板管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R remove(@PathVariable String[] ids) {
        for (String id:ids) {
            if(modGuideTemplateStepService.queryStepsByTemplateId(id).size()!=0){
                return R.fail("当前模板下已挂载引导节点，如需删除请先移除挂载节点");
            }else if(StringUtils.equals(modGuideTemplateService.get(id).getTemplateStatus(),"0")){
                return R.fail("当前模板已发布，如需删除请先禁用该模板");
            }
        }
        return R.status(modGuideTemplateService.deleteModGuideTemplateByIds(ids));
    }


    /**
     * 导出引导模板管理列表
     */
    @PreAuthorize("@ss.hasPermi('mod:GuideTemplate:export')")
    @Log(title = "引导模板管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public R export(ModGuideTemplate modGuideTemplate) {
        List<ModGuideTemplate> list = modGuideTemplateService.findList(modGuideTemplate);
        ExcelUtil<ModGuideTemplate> util = new ExcelUtil<ModGuideTemplate>(ModGuideTemplate.class);
        return util.exportExcel(list, "引导模板管理数据");
    }
	
    /**
     * 根据字典类型查询字典数据信息等其他自定义信息
     */
    @GetMapping(value = "/getInitData/{dictTypes}")
    public R getInitData(@PathVariable String dictTypes) {
        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.putAll(DictUtils.getMultiDictList(dictTypes));
        return R.data(dataMap);
    }

}
