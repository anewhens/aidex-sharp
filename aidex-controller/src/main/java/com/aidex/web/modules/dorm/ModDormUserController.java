package com.aidex.web.modules.dorm;

import java.util.List;
import java.util.HashMap;
import java.util.Map;
import com.aidex.common.annotation.Log;
import com.aidex.common.core.domain.R;
import com.aidex.modules.domain.ModDormBuild;
import com.aidex.modules.service.ModDormBuildService;
import com.aidex.system.service.ISysUserService;
import com.github.pagehelper.PageInfo;
import com.aidex.common.core.page.PageDomain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.aidex.common.enums.BusinessType;
import com.aidex.common.utils.poi.ExcelUtil;
import com.aidex.framework.cache.DictUtils;
import javax.validation.constraints.*;
import org.springframework.web.bind.annotation.*;
import com.aidex.common.core.controller.BaseController;
import org.springframework.validation.annotation.Validated;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import com.aidex.modules.domain.ModDormUser;
import com.aidex.modules.service.ModDormUserService;

/**
 * 宿舍关联人员表Controller
 * @author anewhen
 * @email anewhen@foxmail.com
 * @date 2022-07-18
 */
@RestController
@RequestMapping("/modules/modDormUser")
public class ModDormUserController extends BaseController {

    @Autowired
    private ModDormUserService modDormUserService;

    @Autowired
    private ModDormBuildService modDormBuildService;

    @Autowired
    private ISysUserService userService;

    /**
     * 查询宿舍关联人员表列表
     */
    @PreAuthorize("@ss.hasPermi('modules:modDormUser:list')")
    @GetMapping("/list")
    public R<PageInfo> list(ModDormUser modDormUser, HttpServletRequest request, HttpServletResponse response) {
        modDormUser.setPage(new PageDomain(request, response));
        return R.data(modDormUserService.findPage(modDormUser));
    }

    /**
     * 获取宿舍关联人员表详细信息
     */
    @PreAuthorize("@ss.hasPermi('modules:modDormUser:query')")
    @GetMapping(value = "/{buildId}")
    public R<ModDormUser> detail(@PathVariable("buildId") String buildId) {
        return R.data(modDormUserService.get(buildId));
    }

    /**
     * 新增宿舍关联人员表
     */
    @PreAuthorize("@ss.hasPermi('modules:modDormUser:add')")
    @Log(title = "宿舍关联人员表", businessType = BusinessType.INSERT)
    @PostMapping
    public R add(@RequestBody @Validated  ModDormUser modDormUser) {
        return R.status(modDormUserService.save(modDormUser));
    }

    /**
     * 修改宿舍关联人员表
     */
    @PreAuthorize("@ss.hasPermi('modules:modDormUser:edit')")
    @Log(title = "宿舍关联人员表", businessType = BusinessType.UPDATE)
    @PutMapping
    public R edit(@RequestBody @Validated ModDormUser modDormUser) {
        return R.status(modDormUserService.save(modDormUser));
    }

    /**
     * 删除宿舍关联人员表
     */
    @PreAuthorize("@ss.hasPermi('modules:modDormUser:remove')")
    @Log(title = "宿舍关联人员表", businessType = BusinessType.DELETE)
    @DeleteMapping("/{buildIds}")
    public R remove(@PathVariable String[] ids) {
        return R.status(modDormUserService.deleteModDormUserByIds(ids));
    }


    /**
     * 导出宿舍关联人员表列表
     */
    @PreAuthorize("@ss.hasPermi('modules:modDormUser:export')")
    @Log(title = "宿舍关联人员表", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public R export(ModDormUser modDormUser) {
        List<ModDormUser> list = modDormUserService.findList(modDormUser);
        ExcelUtil<ModDormUser> util = new ExcelUtil<ModDormUser>(ModDormUser.class);
        return util.exportExcel(list, "宿舍关联人员表数据");
    }
	
    /**
     * 根据字典类型查询字典数据信息等其他自定义信息
     */
    @GetMapping(value = "/getInitData/{dictTypes}")
    public R getInitData(@PathVariable String dictTypes) {
        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.putAll(DictUtils.getMultiDictList(dictTypes));
        return R.data(dataMap);
    }

    /*
    * 查询用户历史住宿信息
    */

    @GetMapping(value = "/distributionLog/{userId}")
    public R distributionLog(@PathVariable("userId") String userId) {
        Map res = new HashMap();
        res.put("user",userService.get(userId));
        res.put("dorm",modDormUserService.selectDistributionLog(userId));
        return R.data(res);
    }
    /*
    *公寓分配回撤
     */
    @GetMapping(value = "/dormBack/{userId}")
    public R dormBack(@PathVariable("userId") String userId){
        return R.success();
    }

    /*
    * 公寓分配
    */
    @GetMapping(value = "/distribution/{buildId}/{userId}")
    public R distribution(@PathVariable("buildId") String buildId,@PathVariable("userId") String userId){
        return R.success(modDormUserService.distribution(buildId,userId));
    }
}
