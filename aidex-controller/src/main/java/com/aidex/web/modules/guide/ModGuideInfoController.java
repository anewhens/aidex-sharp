package com.aidex.web.modules.guide;

import java.util.*;

import com.aidex.common.annotation.Log;
import com.aidex.common.core.domain.R;
import com.aidex.common.utils.SecurityUtils;
import com.github.pagehelper.PageInfo;
import com.aidex.common.core.page.PageDomain;

import javax.security.auth.login.LoginContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.aidex.common.enums.BusinessType;
import com.aidex.common.utils.poi.ExcelUtil;
import com.aidex.framework.cache.DictUtils;
import javax.validation.constraints.*;
import org.springframework.web.bind.annotation.*;
import com.aidex.common.core.controller.BaseController;
import org.springframework.validation.annotation.Validated;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import com.aidex.modules.domain.ModGuideInfo;
import com.aidex.modules.service.ModGuideInfoService;

/**
 * 引导模块用户记录Controller
 * @author anewhen
 * @email anewhen@foxmail.com
 * @date 2022-05-23
 */
@RestController
@RequestMapping("/mod/guide/info")
public class ModGuideInfoController extends BaseController {

    @Autowired
    private ModGuideInfoService modGuideInfoService;

    /**
     * 查询引导模块用户记录列表
     */
    @PreAuthorize("@ss.hasPermi('modules:modGuideInfo:list')")
    @GetMapping("/list")
    public R<PageInfo> list(ModGuideInfo modGuideInfo, HttpServletRequest request, HttpServletResponse response) {
        modGuideInfo.setPage(new PageDomain(request, response));
        return R.data(modGuideInfoService.findPage(modGuideInfo));
    }

    /**
     * 获取引导模块用户记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('modules:modGuideInfo:query')")
    @GetMapping(value = "/{userId}")
    public R<ModGuideInfo> detail(@PathVariable("userId") String userId) {
        return R.data(modGuideInfoService.get(userId));
    }

    /**
     * 新增引导模块用户记录
     */
    @PreAuthorize("@ss.hasPermi('modules:modGuideInfo:add')")
    @Log(title = "引导模块用户记录", businessType = BusinessType.INSERT)
    @PostMapping
    public R add(@RequestBody @Validated  ModGuideInfo modGuideInfo) {
        return R.status(modGuideInfoService.save(modGuideInfo));
    }

    /**
     * 修改引导模块用户记录
     */
    @PreAuthorize("@ss.hasPermi('modules:modGuideInfo:edit')")
    @Log(title = "引导模块用户记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public R edit(@RequestBody @Validated ModGuideInfo modGuideInfo) {
        return R.status(modGuideInfoService.save(modGuideInfo));
    }

    /**
     * 删除引导模块用户记录
     */
    @PreAuthorize("@ss.hasPermi('modules:modGuideInfo:remove')")
    @Log(title = "引导模块用户记录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{userIds}")
    public R remove(@PathVariable String[] ids) {
        return R.status(modGuideInfoService.deleteModGuideInfoByIds(ids));
    }


    /**
     * 导出引导模块用户记录列表
     */
    @PreAuthorize("@ss.hasPermi('modules:modGuideInfo:export')")
    @Log(title = "引导模块用户记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public R export(ModGuideInfo modGuideInfo) {
        List<ModGuideInfo> list = modGuideInfoService.findList(modGuideInfo);
        ExcelUtil<ModGuideInfo> util = new ExcelUtil<ModGuideInfo>(ModGuideInfo.class);
        return util.exportExcel(list, "引导模块用户记录数据");
    }
	
    /**
     * 根据字典类型查询字典数据信息等其他自定义信息
     */
    @GetMapping(value = "/getInitData/{dictTypes}")
    public R getInitData(@PathVariable String dictTypes) {
        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.putAll(DictUtils.getMultiDictList(dictTypes));
        return R.data(dataMap);
    }

    /**
     * 批量圈选用户
     * @param ids
     * @param templateId
     * @return
     */
    @PostMapping("/addUserToTemplateBash")
    public R addUserToTemplateBash(@RequestBody ModGuideInfo modal){
        List<ModGuideInfo> modals = new ArrayList<>();
        for (String id:modal.getUserId().split(";")
             ) {
            ModGuideInfo m = new ModGuideInfo();
            m.setTemplateId(modal.getTemplateId());
            m.setStatus("0");
            m.setStep(0L);
            m.setVersion(1);
            m.setCreateBy(SecurityUtils.getUsername());
            m.setCreateDept(SecurityUtils.getLoginUser().getUser().getSysDept().getDeptName());
            m.setCreateTime(new Date());
            m.setId(UUID.randomUUID().toString().replace("-",""));
            m.setUserId(id);
            modals.add(m);
        }
        //清空原始数据
        modGuideInfoService.deleteBashByTemplateId(modal.getTemplateId());
        modGuideInfoService.insertBash(modals);
        return R.success();
    }

}
