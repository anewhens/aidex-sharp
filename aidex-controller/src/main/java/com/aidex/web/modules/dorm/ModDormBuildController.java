package com.aidex.web.modules.dorm;

import java.util.List;
import java.util.HashMap;
import java.util.Map;
import com.aidex.common.annotation.Log;
import com.aidex.common.constant.Constants;
import com.aidex.common.core.domain.R;
import com.aidex.common.core.domain.TreeNode;
import com.aidex.common.utils.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.aidex.common.enums.BusinessType;
import com.aidex.common.utils.poi.ExcelUtil;
import com.aidex.framework.cache.DictUtils;
import javax.validation.constraints.*;
import org.springframework.web.bind.annotation.*;
import com.aidex.common.core.controller.BaseController;
import org.springframework.validation.annotation.Validated;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import com.aidex.modules.domain.ModDormBuild;
import com.aidex.modules.service.ModDormBuildService;

/**
 * dormController
 * @author anewhen
 * @email anewhen@foxmail.com
 * @date 2022-06-24
 */
@RestController
@RequestMapping("/mod/dorm/build")
public class ModDormBuildController extends BaseController {

    @Autowired
    private ModDormBuildService modDormBuildService;

    /**
     * 根据层级展开建筑树表格
     * @param level 展开层级
     * @param id 起始展开ID
     * @return com.aidex.common.core.domain.R
     */
    @PreAuthorize("@ss.hasPermi('system:dept:list')")
    @GetMapping("/list/{level}/{id}")
    public R list(@PathVariable("level") @NotEmpty int level, @PathVariable("id") String id) {
        if (level == 0) {
            level = 2;
        }
        //默认为根节点
        if (StringUtils.isEmpty(id)) {
            id = Constants.TREE_ROOT;
        }
        List<ModDormBuild> builds = modDormBuildService.listDataByLevel(level, id);
        System.out.println(builds);
        return R.data(builds);
    }

//    /**
//     * 查询dorm列表
//     */
//    @PreAuthorize("@ss.hasPermi('modules:modDormBuild:list')")
//    @GetMapping("/list")
//    public R<PageInfo> list(ModDormBuild modDormBuild, HttpServletRequest request, HttpServletResponse response) {
//        modDormBuild.setPage(new PageDomain(request, response));
//        return R.data(modDormBuildService.findPage(modDormBuild));
//    }

    /**
     * 获取dorm详细信息
     */
    @PreAuthorize("@ss.hasPermi('modules:modDormBuild:query')")
    @GetMapping(value = "/{buildCode}")
    public R<ModDormBuild> detail(@PathVariable("buildCode") String buildCode) {
        return R.data(modDormBuildService.get(buildCode));
    }

    /**
     * 新增dorm
     */
    @PreAuthorize("@ss.hasPermi('modules:modDormBuild:add')")
    @Log(title = "dorm", businessType = BusinessType.INSERT)
    @PostMapping
    public R add(@RequestBody @Validated  ModDormBuild modDormBuild) {
        modDormBuildService.save(modDormBuild);
        return R.data(modDormBuild);
    }

    /**
     * 修改dorm
     */
    @PreAuthorize("@ss.hasPermi('modules:modDormBuild:edit')")
    @Log(title = "dorm", businessType = BusinessType.UPDATE)
    @PutMapping
    public R edit(@RequestBody @Validated ModDormBuild modDormBuild) {
        modDormBuildService.save(modDormBuild);
        return R.data(modDormBuild);
    }

    /**
     * 删除dorm
     */
    @PreAuthorize("@ss.hasPermi('modules:modDormBuild:remove')")
    @Log(title = "dorm", businessType = BusinessType.DELETE)
    @DeleteMapping("/{buildCodes}")
    public R remove(@PathVariable String[] ids) {
        return R.status(modDormBuildService.deleteModDormBuildByIds(ids));
    }


    /**
     * 导出dorm列表
     */
    @PreAuthorize("@ss.hasPermi('modules:modDormBuild:export')")
    @Log(title = "dorm", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public R export(ModDormBuild modDormBuild) {
        List<ModDormBuild> list = modDormBuildService.findList(modDormBuild);
        ExcelUtil<ModDormBuild> util = new ExcelUtil<ModDormBuild>(ModDormBuild.class);
        return util.exportExcel(list, "dorm数据");
    }
	
    /**
     * 根据字典类型查询字典数据信息等其他自定义信息
     */
    @GetMapping(value = "/getInitData/{dictTypes}")
    public R getInitData(@PathVariable String dictTypes) {
        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.putAll(DictUtils.getMultiDictList(dictTypes));
        return R.data(dataMap);
    }

    /**
     * 部门选择树
     * @param level 初始展开层级
     * @param id 展开节点ID
     * @return
     */
    @PreAuthorize("@ss.hasPermi('modules:modDormBuild:list')")
    @GetMapping("/listTree/{level}/{id}")
    public R listTree(@NotBlank @PathVariable("level") int level, @PathVariable("id") String id) {
        if (level == 0) {
            level = 2;
        }
        //默认为根节点
        if (StringUtils.isEmpty(id)) {
            id = Constants.TREE_ROOT;
        }
        List<TreeNode> depts = modDormBuildService.buildDormTree(level, id);
        return R.data(depts);
    }

    /**
     * 部门树检索
     */
    @PreAuthorize("@ss.hasPermi('modules:modDormBuild:list')")
    @GetMapping("/search")
    public R search (ModDormBuild dorm)
    {
        List<TreeNode> depts =  modDormBuildService.search(dorm.getBuildName());
        return R.data(depts);
    }

    /**
     * 通用选人页面根据用户ID
     * @return
     */
    @PostMapping(value = { "/getDormInfoByIds"})
    public R getDormInfoByIds(@Validated @RequestBody JSONObject dormIdsObj)
    {
        List<Map<String,Object>> dormtList = modDormBuildService.getDormInfoByIds(dormIdsObj);
        return R.data(dormtList);
    }

    /* 获取当前父节点下最大编号
     * @param parentId
     * @return com.aidex.common.core.domain.R
     */
    @PreAuthorize("@ss.hasPermi('modules:modDormBuild:query')")
    @GetMapping("/findMaxSort/{parentId}")
    public R findMaxSort(@PathVariable("parentId") String parentId) {
        ModDormBuild build = new ModDormBuild();
        build.setParentId(parentId);
        return R.data(modDormBuildService.findMaxSort(build));
    }
}
