package com.aidex.web.modules.attendance;

import java.util.List;
import java.util.HashMap;
import java.util.Map;
import com.aidex.common.annotation.Log;
import com.aidex.common.core.domain.R;
import com.github.pagehelper.PageInfo;
import com.aidex.common.core.page.PageDomain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.aidex.common.enums.BusinessType;
import com.aidex.common.utils.poi.ExcelUtil;
import com.aidex.framework.cache.DictUtils;
import javax.validation.constraints.*;
import org.springframework.web.bind.annotation.*;
import com.aidex.common.core.controller.BaseController;
import org.springframework.validation.annotation.Validated;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import com.aidex.modules.domain.ModAttendanceLog;
import com.aidex.modules.service.ModAttendanceLogService;

/**
 * 考勤记录表Controller
 * @author anewhen
 * @email anewhen@foxmail.com
 * @date 2022-07-26
 */
@RestController
@RequestMapping("/modules/modAttendanceLog")
public class ModAttendanceLogController extends BaseController {

    @Autowired
    private ModAttendanceLogService modAttendanceLogService;

    /**
     * 查询考勤记录表列表
     */
    @PreAuthorize("@ss.hasPermi('modules:modAttendanceLog:list')")
    @GetMapping("/list")
    public R<PageInfo> list(ModAttendanceLog modAttendanceLog, HttpServletRequest request, HttpServletResponse response) {
        modAttendanceLog.setPage(new PageDomain(request, response));
        return R.data(modAttendanceLogService.findPage(modAttendanceLog));
    }

    /**
     * 获取考勤记录表详细信息
     */
    @PreAuthorize("@ss.hasPermi('modules:modAttendanceLog:query')")
    @GetMapping(value = "/{id}")
    public R<ModAttendanceLog> detail(@PathVariable("id") String id) {
        return R.data(modAttendanceLogService.get(id));
    }

    /**
     * 新增考勤记录表
     */
    @PreAuthorize("@ss.hasPermi('modules:modAttendanceLog:add')")
    @Log(title = "考勤记录表", businessType = BusinessType.INSERT)
    @PostMapping
    public R add(@RequestBody @Validated  ModAttendanceLog modAttendanceLog) {
        return R.status(modAttendanceLogService.save(modAttendanceLog));
    }

    /**
     * 修改考勤记录表
     */
    @PreAuthorize("@ss.hasPermi('modules:modAttendanceLog:edit')")
    @Log(title = "考勤记录表", businessType = BusinessType.UPDATE)
    @PutMapping
    public R edit(@RequestBody @Validated ModAttendanceLog modAttendanceLog) {
        return R.status(modAttendanceLogService.save(modAttendanceLog));
    }

    /**
     * 删除考勤记录表
     */
    @PreAuthorize("@ss.hasPermi('modules:modAttendanceLog:remove')")
    @Log(title = "考勤记录表", businessType = BusinessType.DELETE)
    @DeleteMapping("/{userIds}")
    public R remove(@PathVariable String[] ids) {
        return R.status(modAttendanceLogService.deleteModAttendanceLogByIds(ids));
    }


    /**
     * 导出考勤记录表列表
     */
    @PreAuthorize("@ss.hasPermi('modules:modAttendanceLog:export')")
    @Log(title = "考勤记录表", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public R export(ModAttendanceLog modAttendanceLog) {
        List<ModAttendanceLog> list = modAttendanceLogService.findList(modAttendanceLog);
        ExcelUtil<ModAttendanceLog> util = new ExcelUtil<ModAttendanceLog>(ModAttendanceLog.class);
        return util.exportExcel(list, "考勤记录表数据");
    }
	
    /**
     * 根据字典类型查询字典数据信息等其他自定义信息
     */
    @GetMapping(value = "/getInitData/{dictTypes}")
    public R getInitData(@PathVariable String dictTypes) {
        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.putAll(DictUtils.getMultiDictList(dictTypes));
        return R.data(dataMap);
    }

    /**
     * 批量创建缺勤记录
     * @param ids
     * @return
     */
    @PostMapping("/saveBash")
    public R saveBash(@RequestBody String[] ids){
        if(ids==null)
            return R.fail("传入参数为空，本次提交失败");
        modAttendanceLogService.saveBash(ids);
        return R.success();
    }

}
