package com.aidex.web.modules.guide;

import com.aidex.common.core.domain.R;
import com.aidex.common.core.redis.RedisCache;
import com.aidex.common.utils.SecurityUtils;
import com.aidex.common.utils.StringUtils;
import com.aidex.modules.domain.ModGuideInfo;
import com.aidex.modules.domain.ModGuideTemplate;
import com.aidex.modules.domain.ModGuideTemplateStep;
import com.aidex.modules.service.ModGuideInfoService;
import com.aidex.modules.service.ModGuideTemplateService;
import com.aidex.modules.service.ModGuideTemplateStepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 引导模块对外接口控制器
 */
@RestController
@RequestMapping("/mod/guide/api")
public class GuideApiController {

    @Autowired
    private ModGuideTemplateService modGuideTemplateService;

    @Autowired
    private ModGuideTemplateStepService modGuideTemplateStepService;

    @Autowired
    private ModGuideInfoService modGuideInfoService;

    @Autowired
    private RedisCache redisCache;

    /**
     * 根据登录用户获取模板列表
     * @return
     */
    @GetMapping("/list")
    public R getTemplateList(){
        return R.data(modGuideInfoService.queryTemplateListByUserId(SecurityUtils.getUserId()));
    }

    /**
     * 根据模板二维码ID获取模板信息
     * @param code
     * @return
     */
    @GetMapping("/detail")
    public R getStepByTemplateCode(String code){

        Map<String,Object> res = new HashMap<>();

        /**
         * 从缓存中检查该用户是否被圈定
         */
        ModGuideInfo guideInfoCache = guideInfoCache = modGuideInfoService.queryGuideInfoByTemplateIdAndUserId(code, SecurityUtils.getUserId());
        if(guideInfoCache == null){
            return R.fail("[云引导]当前用户不存在引导信息，请联系管理员");
        }


        /**
         * 从缓存中获取模板相关信息
         */
        ModGuideTemplate templateCache = (ModGuideTemplate) redisCache.getCacheObject("guide_template_" + code);
        if(templateCache == null){
            templateCache = modGuideTemplateService.get(code);
            if(templateCache == null){
                return R.fail("[云引导]模板ID不存在，请联系管理员");
            }else{
                redisCache.setCacheObject("guide_template_" + code,templateCache);
            }
        }

        /**
         * 校验登录状态
         */
        if(!StringUtils.equals(templateCache.getTemplateStatus(),"0")){
            return R.fail("[云引导]该模板未上线，请联系管理员");
        }

        /**
         * 从缓存中获取步骤相关信息
         */
        List<ModGuideTemplateStep> templateStepCache = modGuideTemplateStepService.queryStepsByTemplateId(code);
        if(templateStepCache == null){
            return R.fail("[云引导]模板步骤不存在，请联系管理员");
        }


        res.put("info",guideInfoCache);
        res.put("template",templateCache);
        res.put("step",templateStepCache);

        return R.data(res);
    }

    /**
     * 主动扫码结果回传
     * @param t 模板id
     * @param s 步骤id
     * @param r 扫码结果
     * @return
     */
    @GetMapping("/activeSacn")
    public R activeSacn(String t,String s,String r){
        //校验步骤id和扫码结果是否一致
        if(!StringUtils.equals(s,r)){
            return R.fail("请扫描正确的步骤码进行确认");
        }
        //根据模板id及用户，获取当前用户步骤信息
        ModGuideInfo modGuideInfo = modGuideInfoService.queryGuideInfoByTemplateIdAndUserId(t, SecurityUtils.getUserId());
        ModGuideTemplateStep modGuideTemplateStep = modGuideTemplateStepService.get(s);
        if(modGuideInfo==null){
            return R.fail("未查询到相关数据，请联系管理员");
        }else if(modGuideInfo.getStep()+1!=modGuideTemplateStep.getStepSort()){
            return R.fail("存在未完成步骤，请联系管理员");
        }
        //更新当前步骤+1
        modGuideInfo.setStep(modGuideInfo.getStep()+1);
        modGuideInfoService.update(modGuideInfo);
        return R.success();
    }

    /**
     * 用户主动点击完成
     * @param t 模板id
     * @param s 步骤id
     * @return
     */
    @GetMapping("/finish")
    public R finish(String t,String s){
        //根据模板id及用户，获取当前用户步骤信息
        ModGuideInfo modGuideInfo = modGuideInfoService.queryGuideInfoByTemplateIdAndUserId(t, SecurityUtils.getUserId());
        ModGuideTemplateStep modGuideTemplateStep = modGuideTemplateStepService.get(s);
        if(modGuideInfo==null){
            return R.fail("未查询到相关数据，请联系管理员");
        }else if(modGuideInfo.getStep()+1!=modGuideTemplateStep.getStepSort()){
            return R.fail("存在未完成步骤，请联系管理员");
        }
        //更新当前步骤+1
        modGuideInfo.setStep(modGuideInfo.getStep()+1);
        modGuideInfoService.update(modGuideInfo);
        return R.success();
    }

    /**
     * 变更引导记录为已完成（用于变更guide_info的status）
     * @param t
     * @return
     */
    @GetMapping("/complete")
    public R complete(String t){
        if (StringUtils.isEmpty(t)) {
            return R.fail("参数异常，请联系管理员");
        }
        ModGuideInfo modGuideInfo = modGuideInfoService.queryGuideInfoByTemplateIdAndUserId(t, SecurityUtils.getUserId());
        modGuideInfo.setStatus("1");
        modGuideInfoService.update(modGuideInfo);
        return R.success();
    }

    /**
     * 人工值守步骤
      * @param templateId
     * @param stepCode
     * @param userId
     * @return
     */
    @GetMapping("/humanWatch")
    public R humanWatch(String t,String s,String u){
        //获取步骤信息及及进度信息
        ModGuideInfo modGuideInfo = modGuideInfoService.queryGuideInfoByTemplateIdAndUserId(t, u);
        ModGuideTemplate template = modGuideTemplateService.get(t);
        ModGuideTemplateStep modGuideTemplateStep = modGuideTemplateStepService.get(s);
        if(modGuideInfo==null){
            return R.fail("未查询到相关数据，请联系管理员");
        }else if(modGuideInfo.getStep()+1!=modGuideTemplateStep.getStepSort()){
            return R.fail("存在未完成步骤，请联系管理员");
        }
        //更新当前步骤+1
        modGuideInfo.setStep(modGuideInfo.getStep()+1);
        modGuideInfoService.update(modGuideInfo);
        return R.success();

    }
}
