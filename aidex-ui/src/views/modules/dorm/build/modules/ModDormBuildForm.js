import AntModal from '@/components/pt/dialog/AntModal'
import { getModDormBuild, addModDormBuild, updateModDormBuild, findMaxSort } from '@/api/modules/ModDormBuild'
import selectDorm from '@/components/pt/selectDorm/SelectDorm'

export default {
  name: 'CreateForm',
  props: {
    dormTypeOptions: {
      type: Array,
      required: true
    },

    statusOptions: {
      type: Array,
      required: true
    }

  },
  components: {
    AntModal,
    selectDorm
  },
  data () {
    return {
      selectScope: 'all',
      open: false,
      parentIdShow: false,
      closeDialog: true,
      spinning: false,
      buildTypeEnableValue: [],
      delayTime: 100,
      labelCol: { span: 4 },
      wrapperCol: { span: 14 },
      loading: false,
      customStyle: 'background: #fff;ssborder-radius: 4px;margin-bottom: 24px;border: 0;overflow: hidden',
      disabled: false,
      total: 0,
      id: undefined,
      formTitle: '添加dorm',
      // 表单参数
      form: {},
      rules: {
      }
    }
  },
  filters: {},
  created () {},
  computed: {},
  watch: {},
  mounted () {},
  methods: {
    onSelectDorm (result) {
      this.getBuildTypeEnableValue(result.types)
    },
    getBuildTypeEnableValue (parentBuildType) {
      const id = this.form.id
      /* if (parentDeptType === null) {
        this.deptTypeEnableValue = this.deptTypeOptions.filter(function (item) { return item.dictValue === 'org' })
          .map(function (item) { return item.dictValue })
      } else if (this.hasChild) {
        this.deptTypeEnableValue = this.deptTypeOptions.filter(function (item) { return item.dictValue === 'company' })
          .map(function (item) { return item.dictValue })
      } else if (parentDeptType === 'org' || parentDeptType === 'company' || parentDeptType === '') {
        this.deptTypeEnableValue = this.deptTypeOptions.filter(function (item) { return item.dictValue !== 'org' })
          .map(function (item) { return item.dictValue })
      } else {
        this.deptTypeEnableValue = this.deptTypeOptions.filter(function (item) { return item.dictValue === 'dept' })
          .map(function (item) { return item.dictValue })
      } */
      if (parentBuildType === 'build' || parentBuildType === 'unit') {
        this.buildTypeEnableValue = this.dormTypeOptions.filter(function (item) { return item.dictValue !== 'build' })
          .map(function (item) { return item.dictValue })
      } else {
        this.buildTypeEnableValue = this.dormTypeOptions.filter(function (item) { return item.dictValue === 'room' })
          .map(function (item) { return item.dictValue })
      }
      if (id !== null && id !== '' && id !== 'undefined' && id !== undefined) {
        // 编辑页面
        if (parentBuildType === null) {
          this.buildTypeEnableValue = this.dormTypeOptions.filter(function (item) { return item.dictValue === 'build' })
            .map(function (item) { return item.dictValue })
        }
        // 编辑页面当切换上级部门后需要判断当前部门类型是否在可选集合，如果在类型保持不变，如果不在需要重新赋值
        const buildType = this.form.buildType
        const selectBuildType = this.buildTypeEnableValue.filter(function (item) { return item === buildType })
        this.form.buildType = selectBuildType.length === 0 ? this.buildTypeEnableValue[0] : buildType
      } else {
        // 添加页面
        this.form.buildType = this.buildTypeEnableValue[0]
      }
    },
    onBuildTypeChange (item) {
      console.log(item.target)
      if (item.target.value === 'depart') {
        this.selectScope = 'nonDept'
      } else {
        this.selectScope = 'all'
      }
    },
    onClose () {
      this.open = false
      this.reset()
      this.$emit('close')
    },
    // 取消按钮
    cancel () {
      this.open = false
      this.reset()
      this.$emit('close')
    },
    // 表单重置
    reset () {
      this.form = {
        id: undefined,
        buildCode: undefined,

        buildName: undefined,

        leader: undefined,

        phone: undefined,

        buildType: undefined,

        address: undefined,

        deptPinyin: undefined,

        treeSort: undefined,

        status: '0',

        members: 0

      }
    },
    /** 新增按钮操作 */
    handleAdd (row) {
      this.parentIdShow = true
      this.oldParentId = ''
      this.buildTypeEnableValue = this.dormTypeOptions.map(function (item) { return item.dictValue })
      if (row !== undefined) {
        this.currentRow = row
        this.oldParentId = row.id
        this.form.parentId = { ids: row.id, names: row.buildName }
        this.getBuildTypeEnableValue(row.buildType)
      }
      /** 获取最大编号 */
      findMaxSort(row !== undefined ? row.id : '0').then(response => {
        this.form.treeSort = response.data
      })
      this.$emit('getTreeselect')
      this.formTitle = '添加机构信息'
      this.open = true
      // this.reset()
      // this.open = true
      // this.formTitle = '添加dorm'
    },
    /** 修改按钮操作 */
    handleUpdate (row) {
      this.reset()
      this.open = true
      this.spinning = !this.spinning
      this.currentRow = row
      const modDormBuildId = row.id
      getModDormBuild(modDormBuildId).then(response => {
        this.form = response.data
        this.formTitle = '修改dorm'
        this.spinning = !this.spinning
      })
    },
    setNodeData (data) {
      // console.log(this.currentRow)
      // console.log(data)
      this.currentRow.buildName = data.buildName
      this.currentRow.buildCode = data.buildCode
      this.currentRow.leader = data.leader
      this.currentRow.phone = data.phone
      this.currentRow.email = data.email
      this.currentRow.status = data.status
      this.currentRow.treeSort = data.treeSort
      this.currentRow.createTime = data.createTime
      this.currentRow.buildType = data.buildType
    },
    /** 提交按钮 */
    submitForm: function () {
      this.$refs.form.validate(valid => {
        if (valid) {
          const saveForm = JSON.parse(JSON.stringify(this.form))
          if (this.form.id === undefined && this.form.parentId !== undefined) {
            saveForm.parentId = this.form.parentId.ids
          }
          /** 设置状态初始值为正常 */
          saveForm.status = 0
          /** 设置如果非room类型，人数置空
           */
          if (this.form.buildType !== 'room') {
            this.form.members = 0
          }
          if (this.form.id !== undefined) {
            updateModDormBuild(saveForm).then(response => {
              this.$message.success(
                '修改成功',
                3
              )
              if (this.oldParentId !== this.form.parentId.ids) {
                // 如果修改父节点则刷新树
                this.$emit('ok')
              } else {
                this.setNodeData(response.data)
              }
              this.cancel()
            })
          } else {
            addModDormBuild(saveForm).then(response => {
              this.$message.success(
                '新增成功',
                3
              )
              // 修改父节点后刷新整个树，如果直接添加子节点不更换父节点则追加节点
              console.log(this.form)
              if (this.oldParentId !== this.form.parentId.ids) {
                // 如果修改父节点则刷新树
                this.$emit('ok')
              } else {
                this.appendTreeNode(this.currentRow, response.data)
              }
              this.cancel()
            })
          }
        } else {
          return false
        }
      })
      // this.closeDialog = closeDialog
      // this.disabled = true
      // this.$refs.form.validate(valid => {
      //   if (valid) {
      //     const saveForm = JSON.parse(JSON.stringify(this.form))
      //     if (this.form.id !== undefined) {
      //       updateModDormBuild(saveForm).then(response => {
      //         this.$message.success('更新成功', 3)
      //         this.open = false
      //         this.$emit('ok')
      //         this.$emit('close')
      //         this.disabled = false
      //       })
      //     } else {
      //       addModDormBuild(saveForm).then(response => {
      //         this.$message.success('新增成功', 3)
      //         this.open = false
      //         this.$emit('ok')
      //         this.$emit('close')
      //         this.disabled = false
      //       })
      //     }
      //   } else {
      //     this.disabled = false
      //     return false
      //   }
      // })
    },
    back () {
      const index = '/modules/moddormbuild/index'
      this.$router.push(index)
    }

  }
}
