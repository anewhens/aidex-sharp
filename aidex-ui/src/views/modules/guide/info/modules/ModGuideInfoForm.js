import AntModal from '@/components/pt/dialog/AntModal'
import SelectUser from '@/components/pt/selectUser/SelectUser'
import { getModGuideInfo, addUserToTemplateBash } from '@/api/modules/ModGuideInfo'

export default {
  name: 'CreateForm',
  props: {
    statusOptions: {
      type: Array,
      required: true
    }

  },
  components: {
    AntModal,
    SelectUser
  },
  data () {
    return {
      selectUsers: [],
      open: false,
      closeDialog: true,
      spinning: false,
      delayTime: 100,
      labelCol: { span: 4 },
      wrapperCol: { span: 14 },
      loading: false,
      disabled: false,
      total: 0,
      id: undefined,
      templateId: undefined,
      formTitle: '添加引导模块用户记录',
      // 表单参数
      form: {},
      rules: {
      }
    }
  },
  filters: {},
  created () {},
  computed: {},
  watch: {},
  mounted () {},
  methods: {
    onClose () {
      this.open = false
      this.reset()
      this.$emit('close')
    },
    // 取消按钮
    cancel () {
      this.open = false
      this.reset()
      this.$emit('close')
    },
    // 表单重置
    reset () {
      this.form = {
        id: undefined,
        step: undefined,

        status: '0'

      }
    },
    /** 新增按钮操作 */
    handleAdd (record) {
      this.reset()
      this.templateId = record.id
      this.open = true
      this.formTitle = '圈选用户'
    },
    /** 修改按钮操作 */
    handleUpdate (row) {
      this.reset()
      this.open = true
      this.spinning = !this.spinning
      const modGuideInfoId = row.id
      getModGuideInfo(modGuideInfoId).then(response => {
        this.form = response.data
        this.formTitle = '修改引导模块用户记录'
        this.spinning = !this.spinning
      })
    },
    /** 提交按钮 */
    submitForm: function (closeDialog) {
      this.closeDialog = closeDialog
      this.disabled = true
      this.$refs.form.validate(valid => {
        if (valid) {
          this.form.userId = this.form.selectUsers.ids
          this.form.templateId = this.templateId
          const saveForm = JSON.parse(JSON.stringify(this.form))
          if (this.form.id !== undefined) {
            // updateModGuideInfo(saveForm).then(response => {
            //   this.$message.success('更新成功', 3)
            //   this.open = false
            //   this.$emit('ok')
            //   this.$emit('close')
            //   this.disabled = false
            // })
          } else {
            addUserToTemplateBash(saveForm).then(response => {
              this.$message.success('新增成功', 3)
              this.open = false
              this.$emit('ok')
              this.$emit('close')
              this.disabled = false
            })
          }
        } else {
          this.disabled = false
          return false
        }
      })
    },
    back () {
      const index = '/modules/modguideinfo/index'
      this.$router.push(index)
    }

  }
}
