import AntModal from '@/components/pt/dialog/AntModal'

import {
  listByTemplateId,
  getModGuideTemplateStep,
  addModGuideTemplateStep,
  updateModGuideTemplateStep
} from '@/api/modules/ModGuideTemplateStep'

export default {
  name: 'CreateForm',
  props: {
    stepTypeOptions: {
      type: Array,
      required: true
    }

  },
  components: {
    AntModal

  },
  data () {
    return {
      open: false,
      closeDialog: true,
      spinning: false,
      delayTime: 100,
      labelCol: { span: 4 },
      wrapperCol: { span: 14 },
      loading: false,
      disabled: false,
      total: 0,
      id: undefined,
      currentTemplateId: undefined,
      formTitle: '添加引导模板步骤',
      stepArr: [],
      // 表单参数
      form: {},
      rules: {
        stepName: [{ required: true, message: '步骤名称不能为空', trigger: 'blur' }],

        stepSort: [{ required: true, message: '排序不能为空', trigger: 'blur' }],

        stepType: [{ required: true, message: '步骤类型1人工值守2主动扫码3点击跳过不能为空', trigger: 'blur' }],

        stepDescription: [{ required: true, message: '步骤描述不能为空', trigger: 'blur' }],

        remark: [{ required: true, message: '不能为空', trigger: 'blur' }]

      }
    }
  },
  filters: {},
  created () {
  },
  computed: {},
  watch: {},
  mounted () {},
  methods: {
    // 构造人工值守二维码参数
    constructQRObj(tid, sid, sname) {
      var obj = {}
      obj.tid = tid
      obj.sid = sid
      obj.sname = sname
      return encodeURIComponent(JSON.stringify(obj))
    },
    onClose () {
      this.open = false
      this.reset()
      this.$emit('close')
    },
    // 取消按钮
    cancel () {
      this.open = false
      this.reset()
      this.$emit('close')
    },
    // 表单重置
    reset () {
      this.form = {
        id: undefined,
        stepName: undefined,

        stepSort: undefined,

        stepType: undefined,

        qrCode: undefined,

        stepDescription: undefined,

        remark: undefined

      }
    },
    /** 新增按钮操作 */
    handleAdd (currentTemplateId) {
      this.reset()
      this.open = true
      this.formTitle = '添加引导模板步骤'
      this.currentTemplateId = currentTemplateId
    },
    /** 修改按钮操作 */
    handleUpdate (row) {
      this.reset()
      this.open = true
      this.spinning = !this.spinning
      const GuideTemplateStepId = row.id
      getModGuideTemplateStep(GuideTemplateStepId).then(response => {
        this.form = response.data
        this.formTitle = '修改引导模板步骤'
        this.spinning = !this.spinning
      })
    },
    /** 获取节点列表 */
    handleStepList (currentId) {
      listByTemplateId(currentId).then(res => {
        this.stepArr = res.data
      })
      // this.reset()
      this.open = true
      this.spinning = !this.spinning
      // const GuideTemplateStepId = row.id
      this.formTitle = '引导步骤总览'
      this.currentTemplateId = currentId
      // getModGuideTemplateStep(GuideTemplateStepId).then(response => {
      //   this.form = response.data
      //   this.formTitle = '修改引导模板步骤'
      //   this.spinning = !this.spinning
      // })
    },
    /** 提交按钮 */
    submitForm: function (closeDialog) {
      this.closeDialog = closeDialog
      this.disabled = true
      this.form.templateId = this.currentTemplateId
      this.$refs.form.validate(valid => {
        if (valid) {
          const saveForm = JSON.parse(JSON.stringify(this.form))
          if (this.form.id !== undefined) {
            updateModGuideTemplateStep(saveForm).then(response => {
              this.$message.success('更新成功', 3)
              this.open = false
              this.$emit('ok')
              this.$emit('close')
              this.disabled = false
            })
          } else {
            addModGuideTemplateStep(saveForm).then(response => {
              this.$message.success('新增成功', 3)
              this.open = false
              this.$emit('ok')
              this.$emit('close')
              this.disabled = false
            })
          }
        } else {
          this.disabled = false
          return false
        }
      })
    },
    back () {
      const index = '/modules/guidetemplatestep/index'
      this.$router.push(index)
    }

  }
}
