import AntModal from '@/components/pt/dialog/AntModal'
import { getModGuideTemplate, addModGuideTemplate, updateModGuideTemplate } from '@/api/modules/ModGuideTemplate'

export default {
  name: 'CreateForm',
  props: {
    templateStatusOptions: {
      type: Array,
      required: true
    }
  },
  components: {
    AntModal
  },
  data () {
    return {
      open: false,
      closeDialog: true,
      spinning: false,
      delayTime: 100,
      labelCol: { span: 4 },
      wrapperCol: { span: 14 },
      loading: false,
      disabled: false,
      total: 0,
      id: undefined,
      formTitle: '添加引导模板管理',
      // 表单参数
      form: {},
      rules: {
        templateName: [{ required: true, message: '模板名称不能为空', trigger: 'blur' }],

        templateStatus: [{ required: true, message: '模板状态 0正在使用 1禁用不能为空', trigger: 'blur' }]

      }
    }
  },
  filters: {},
  created () {},
  computed: {},
  watch: {},
  mounted () {},
  methods: {
    onClose () {
      this.open = false
      this.reset()
      this.$emit('close')
    },
    // 取消按钮
    cancel () {
      this.open = false
      this.reset()
      this.$emit('close')
    },
    // 表单重置
    reset () {
      this.form = {
        id: undefined,
        templateName: undefined,

        templateStatus: undefined

      }
    },
    /** 新增按钮操作 */
    handleAdd () {
      this.reset()
      this.open = true
      this.formTitle = '添加引导模板管理'
    },
    /** 复制按钮操作 */
    handleCopy (id) {
      this.formTitle = '复制引导模板管理'
      this.reset()
      this.open = true
      this.spinning = !this.spinning
      const GuideTemplateId = id
      getModGuideTemplate(GuideTemplateId).then(response => {
        this.form = response.data
        this.form.id = undefined
        this.spinning = !this.spinning
      })
    },
    /** 修改按钮操作 */
    handleUpdate (row) {
      this.reset()
      this.open = true
      this.spinning = !this.spinning
      const GuideTemplateId = row.id
      getModGuideTemplate(GuideTemplateId).then(response => {
        this.form = response.data
        this.formTitle = '修改引导模板管理'
        this.spinning = !this.spinning
      })
    },
    /** 提交按钮 */
    submitForm: function (closeDialog) {
      this.closeDialog = closeDialog
      this.disabled = true
      this.$refs.form.validate(valid => {
        if (valid) {
          const saveForm = JSON.parse(JSON.stringify(this.form))
          if (this.form.id !== undefined) {
            updateModGuideTemplate(saveForm).then(response => {
              this.$message.success('更新成功', 3)
              this.open = false
              this.$emit('ok')
              this.$emit('close')
              this.disabled = false
            })
          } else {
            addModGuideTemplate(saveForm).then(response => {
              this.$message.success('新增成功', 3)
              this.open = false
              this.$emit('ok')
              this.$emit('close')
              this.disabled = false
            })
          }
        } else {
          this.disabled = false
          return false
        }
      })
    },
    back () {
      const index = '/mode/guidetemplate/index'
      this.$router.push(index)
    }

  }
}
