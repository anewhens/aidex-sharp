import AntModal from '@/components/pt/dialog/AntModal'
import { getModAttendanceLog, addModAttendanceLog, updateModAttendanceLog } from '@/api/modules/ModAttendanceLog'

export default {
  name: 'CreateForm',
  props: {

  },
  components: {
    AntModal

  },
  data () {
    return {
      open: false,
      closeDialog: true,
      spinning: false,
      delayTime: 100,
      labelCol: { span: 4 },
      wrapperCol: { span: 14 },
      loading: false,
      disabled: false,
      total: 0,
      id: undefined,
      formTitle: '添加考勤记录表',
      // 表单参数
      form: {},
      rules: {
      }
    }
  },
  filters: {},
  created () {},
  computed: {},
  watch: {},
  mounted () {},
  methods: {
    onClose () {
      this.open = false
      this.reset()
      this.$emit('close')
    },
    // 取消按钮
    cancel () {
      this.open = false
      this.reset()
      this.$emit('close')
    },
    // 表单重置
    reset () {
      this.form = {
        id: undefined,
        userId: undefined

      }
    },
    /** 新增按钮操作 */
    handleAdd () {
      this.reset()
      this.open = true
      this.formTitle = '添加考勤记录表'
    },
    /** 修改按钮操作 */
    handleUpdate (row) {
      this.reset()
      this.open = true
      this.spinning = !this.spinning
      const modAttendanceLogId = row.id
      getModAttendanceLog(modAttendanceLogId).then(response => {
        this.form = response.data
        this.formTitle = '修改考勤记录表'
        this.spinning = !this.spinning
      })
    },
    /** 提交按钮 */
    submitForm: function (closeDialog) {
      this.closeDialog = closeDialog
      this.disabled = true
      this.$refs.form.validate(valid => {
        if (valid) {
          const saveForm = JSON.parse(JSON.stringify(this.form))
          if (this.form.id !== undefined) {
            updateModAttendanceLog(saveForm).then(response => {
              this.$message.success('更新成功', 3)
              this.open = false
              this.$emit('ok')
              this.$emit('close')
              this.disabled = false
            })
          } else {
            addModAttendanceLog(saveForm).then(response => {
              this.$message.success('新增成功', 3)
              this.open = false
              this.$emit('ok')
              this.$emit('close')
              this.disabled = false
            })
          }
        } else {
          this.disabled = false
          return false
        }
      })
    },
    back () {
      const index = '/modules/modattendancelog/index'
      this.$router.push(index)
    }

  }
}
