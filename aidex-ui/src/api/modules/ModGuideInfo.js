import request from '@/utils/request'

// 查询引导模块用户记录列表
export function listModGuideInfo (query) {
  return request({
    url: '/mod/guide/info/list',
    method: 'get',
    params: query
  })
}

// 查询引导模块用户记录详细
export function getModGuideInfo (userId) {
  return request({
    url: '/mod/guide/info/' + userId,
    method: 'get'
  })
}

// 新增引导模块用户记录
export function addModGuideInfo (data) {
  return request({
    url: '/mod/guide/info',
    method: 'post',
    data: data
  })
}

// 批量圈选用户
export function addUserToTemplateBash (data) {
  return request({
    url: '/mod/guide/info/addUserToTemplateBash',
    method: 'post',
    data: data
  })
}

// 修改引导模块用户记录
export function updateModGuideInfo (data) {
  return request({
    url: '/mod/guide/info',
    method: 'put',
    data: data
  })
}

// 删除引导模块用户记录
export function delModGuideInfo (userId) {
  return request({
    url: '/mod/guide/info/' + userId,
    method: 'delete'
  })
}

// 导出引导模块用户记录
export function exportModGuideInfo (query) {
  return request({
    url: '/mod/guide/info/export',
    method: 'get',
    params: query
  })
}

// 获取初始化数据
export function getInitData (dictTypes) {
  return request({
    url: '/mod/guide/info/getInitData/' + dictTypes,
    method: 'get'
  })
}
