import request from '@/utils/request'

// 查询宿舍关联人员表列表
export function listModDormUser (query) {
  return request({
    url: '/modules/modDormUser/list',
    method: 'get',
    params: query
  })
}

// 查询宿舍关联人员表详细
export function getModDormUser (buildId) {
  return request({
    url: '/modules/modDormUser/' + buildId,
    method: 'get'
  })
}

// 新增宿舍关联人员表
export function addModDormUser (data) {
  return request({
    url: '/modules/modDormUser',
    method: 'post',
    data: data
  })
}

// 修改宿舍关联人员表
export function updateModDormUser (data) {
  return request({
    url: '/modules/modDormUser',
    method: 'put',
    data: data
  })
}

// 删除宿舍关联人员表
export function delModDormUser (buildId) {
  return request({
    url: '/modules/modDormUser/' + buildId,
    method: 'delete'
  })
}

// 导出宿舍关联人员表
export function exportModDormUser (query) {
  return request({
    url: '/modules/modDormUser/export',
    method: 'get',
    params: query
  })
}

// 获取初始化数据
export function getInitData (dictTypes) {
  return request({
    url: '/modules/modDormUser/getInitData/' + dictTypes,
    method: 'get'
  })
}

// 获取宿舍分派记录
export function distributionLog (userId) {
  return request({
    url: '/modules/modDormUser/distributionLog/' + userId,
    method: 'get'
  })
}

// 宿舍分配回撤
export function dormBack (userId) {
  return request({
    url: '/modules/modDormUser/dormBack/' + userId,
    method: 'get'
  })
}

// 宿舍分配
export function distribution (buildId, userId) {
  return request({
    url: '/modules/modDormUser/distribution/' + buildId + '/' + userId,
    method: 'get'
  })
}
