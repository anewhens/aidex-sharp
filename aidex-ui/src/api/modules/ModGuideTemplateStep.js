import request from '@/utils/request'

// 查询引导模板步骤列表
export function listModGuideTemplateStep (query) {
  return request({
    url: '/mod/guide/template/step/list',
    method: 'get',
    params: query
  })
}

// 根据模板ID查询步骤清单
export function listByTemplateId (query) {
  return request({
    url: '/mod/guide/template/step/listByTemplateId?id=' + query,
    method: 'get'
  })
}

// 查询引导模板步骤详细
export function getModGuideTemplateStep (stepName) {
  return request({
    url: '/mod/guide/template/step/' + stepName,
    method: 'get'
  })
}

// 新增引导模板步骤
export function addModGuideTemplateStep (data) {
  return request({
    url: '/mod/guide/template/step',
    method: 'post',
    data: data
  })
}

// 修改引导模板步骤
export function updateModGuideTemplateStep (data) {
  return request({
    url: '/mod/guide/template/step',
    method: 'put',
    data: data
  })
}

// 删除引导模板步骤
export function delModGuideTemplateStep (stepName) {
  return request({
    url: '/mod/guide/template/step/' + stepName,
    method: 'delete'
  })
}

// 导出引导模板步骤
export function exportModGuideTemplateStep (query) {
  return request({
    url: '/mod/guide/template/step/export',
    method: 'get',
    params: query
  })
}

// 获取初始化数据
export function getInitData (dictTypes) {
  return request({
    url: '/mod/guide/template/step/getInitData/' + dictTypes,
    method: 'get'
  })
}
