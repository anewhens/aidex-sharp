import request from '@/utils/request'

// 查询dorm列表
export function listModDormBuild (query) {
  return request({
    url: '/mod/dorm/build/list',
    method: 'get',
    params: query
  })
}

// 查询dorm详细
export function getModDormBuild (buildCode) {
  return request({
    url: '/mod/dorm/build/' + buildCode,
    method: 'get'
  })
}

// 新增dorm
export function addModDormBuild (data) {
  return request({
    url: '/mod/dorm/build',
    method: 'post',
    data: data
  })
}

// 修改dorm
export function updateModDormBuild (data) {
  return request({
    url: '/mod/dorm/build',
    method: 'put',
    data: data
  })
}

// 删除dorm
export function delModDormBuild (buildCode) {
  return request({
    url: '/mod/dorm/build/' + buildCode,
    method: 'delete'
  })
}

// 导出dorm
export function exportModDormBuild (query) {
  return request({
    url: '/mod/dorm/build/export',
    method: 'get',
    params: query
  })
}

// 获取初始化数据
export function getInitData (dictTypes) {
  return request({
    url: '/mod/dorm/build/getInitData/' + dictTypes,
    method: 'get'
  })
}

// 查询建筑物列表
export function listDrom (query, id, expandLevel) {
  if (id == null || id === '') {
    id = '0'
  }
  if (expandLevel == null || expandLevel === '') {
    expandLevel = '1'
  }
  return request({
    url: '/mod/dorm/build/list/' + expandLevel + '/' + id,
    method: 'get',
    params: query
  })
}

// 查询建筑树列表（排除当前节点及子节点）
export function listDormTree (dormId, expandLevel) {
  if (dormId == null || dormId === '') {
    dormId = '0'
  }
  if (expandLevel == null || expandLevel === '') {
    expandLevel = '1'
  }
  return request({
    url: '/mod/dorm/build/listTree/' + expandLevel + '/' + dormId,
    method: 'get'
  })
}

// 宿舍树检索
export function searchDorm (searchInfo) {
  return request({
    url: '/mod/dorm/build/search',
    method: 'get',
    params: searchInfo
  })
}

// 查询宿舍详细
export function getDormInfoByIds (userIds) {
  return request({
    url: '/mod/dorm/build/getDeptInfoByIds',
    method: 'post',
    data: userIds
  })
}

// 查询部门下拉树结构
export function findMaxSort (parentId) {
  return request({
    url: '/mod/dorm/build/findMaxSort/' + parentId,
    method: 'get'
  })
}
