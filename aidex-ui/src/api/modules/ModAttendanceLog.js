import request from '@/utils/request'

// 查询考勤记录表列表
export function listModAttendanceLog (query) {
  return request({
    url: '/modules/modAttendanceLog/list',
    method: 'get',
    params: query
  })
}

// 查询考勤记录表详细
export function getModAttendanceLog (userId) {
  return request({
    url: '/modules/modAttendanceLog/' + userId,
    method: 'get'
  })
}

// 新增考勤记录表
export function addModAttendanceLog (data) {
  return request({
    url: '/modules/modAttendanceLog',
    method: 'post',
    data: data
  })
}

// 新增考勤记录表
export function saveBash (data) {
  return request({
    url: '/modules/modAttendanceLog/saveBash',
    method: 'post',
    data: data
  })
}

// 修改考勤记录表
export function updateModAttendanceLog (data) {
  return request({
    url: '/modules/modAttendanceLog',
    method: 'put',
    data: data
  })
}

// 删除考勤记录表
export function delModAttendanceLog (userId) {
  return request({
    url: '/modules/modAttendanceLog/' + userId,
    method: 'delete'
  })
}

// 导出考勤记录表
export function exportModAttendanceLog (query) {
  return request({
    url: '/modules/modAttendanceLog/export',
    method: 'get',
    params: query
  })
}

// 获取初始化数据
export function getInitData (dictTypes) {
  return request({
    url: '/modules/modAttendanceLog/getInitData/' + dictTypes,
    method: 'get'
  })
}
