import request from '@/utils/request'

// 查询引导模板管理列表
export function listModGuideTemplate (query) {
  return request({
    url: '/mod/guide/template/list',
    method: 'get',
    params: query
  })
}

// 查询引导模板管理详细
export function getModGuideTemplate (id) {
  return request({
    url: '/mod/guide/template/' + id,
    method: 'get'
  })
}

// 新增引导模板管理
export function addModGuideTemplate (data) {
  return request({
    url: '/mod/guide/template',
    method: 'post',
    data: data
  })
}

// 修改引导模板管理
export function updateModGuideTemplate (data) {
  return request({
    url: '/mod/guide/template',
    method: 'put',
    data: data
  })
}

// 删除引导模板管理
export function delModGuideTemplate (id) {
  return request({
    url: '/mod/guide/template/' + id,
    method: 'delete'
  })
}

// 导出引导模板管理
export function exportModGuideTemplate (query) {
  return request({
    url: '/mod/guide/template/export',
    method: 'get',
    params: query
  })
}

// 获取初始化数据
export function getInitData (dictTypes) {
  return request({
    url: '/mod/guide/template/getInitData/' + dictTypes,
    method: 'get'
  })
}
