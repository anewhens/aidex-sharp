package com.aidex.modules.service;

import com.aidex.common.core.domain.R;
import com.aidex.common.core.service.BaseService;
import com.aidex.modules.domain.ModDormUser;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Map;

/**
 * 宿舍关联人员表Service接口
 * @author anewhen
 * @email anewhen@foxmail.com
 * @date 2022-07-18
 */
public interface ModDormUserService extends BaseService<ModDormUser> {

    /**
     * 批量删除宿舍关联人员表
     * @param ids 需要删除的宿舍关联人员表ID集合
     * @return 结果
     */
    public int deleteModDormUserByIds(String[] ids);

    public int update(ModDormUser modDormUser);

    /*分配宿舍*/
    public String distribution(String buildId,String userId);

    /**
     * 根据用户ID查询分配日志
     * @param userId
     * @return
     */
    public List<Map> selectDistributionLog(String userId);

    /**
     * 公寓分配回撤
     * @param userId
     */
    public void dormBack( String userId);
}
