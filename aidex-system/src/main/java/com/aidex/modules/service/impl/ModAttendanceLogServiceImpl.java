package com.aidex.modules.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.aidex.common.utils.SecurityUtils;
import com.aidex.common.utils.uuid.UUID;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.github.pagehelper.PageInfo;
import com.aidex.common.core.domain.BaseEntity;
import com.aidex.common.core.service.BaseServiceImpl;
import com.aidex.modules.mapper.ModAttendanceLogMapper;
import com.aidex.modules.domain.ModAttendanceLog;
import com.aidex.modules.service.ModAttendanceLogService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 考勤记录表Service业务层处理
 * @author anewhen
 * @email anewhen@foxmail.com
 * @date 2022-07-26
 */
@Service
@Transactional(readOnly = true)
public class ModAttendanceLogServiceImpl extends BaseServiceImpl<ModAttendanceLogMapper, ModAttendanceLog> implements ModAttendanceLogService {

    private static final Logger log = LoggerFactory.getLogger(ModAttendanceLogServiceImpl.class);

    /**
     * 获取单条数据
     * @param modAttendanceLog 考勤记录表
     * @return 考勤记录表
     */
    @Override
    public ModAttendanceLog get(ModAttendanceLog modAttendanceLog) {
        ModAttendanceLog dto = super.get(modAttendanceLog);
        return dto;
    }

    /**
     * 获取单条数据
     * @param id 考勤记录表id
     * @return 考勤记录表
     */
    @Override
    public ModAttendanceLog get(String id) {
        ModAttendanceLog dto = super.get(id);
        return dto;
    }

    /**
     * 查询考勤记录表列表
     * @param modAttendanceLog 考勤记录表
     * @return 考勤记录表
     */
    @Override
    public List<ModAttendanceLog> findList(ModAttendanceLog modAttendanceLog) {
		List<ModAttendanceLog> modAttendanceLogList = super.findList(modAttendanceLog);
        return modAttendanceLogList;
    }

    /**
     * 分页查询考勤记录表列表
     * @param modAttendanceLog 考勤记录表
     * @return 考勤记录表
     */
    @Override
    public PageInfo<ModAttendanceLog> findPage(ModAttendanceLog modAttendanceLog) {
		PageInfo<ModAttendanceLog> page = super.findPage(modAttendanceLog);
        return page;
    }

    /**
     * 保存考勤记录表
     * @param modAttendanceLog
     * @return 结果
     */
    @Override
    public boolean save(ModAttendanceLog modAttendanceLog) {
        return super.save(modAttendanceLog);
    }

    /**
     * 删除考勤记录表信息
     * @param modAttendanceLog
     * @return 结果
     */
    @Override
    public boolean remove(ModAttendanceLog modAttendanceLog) {
        return super.remove(modAttendanceLog);
    }

    /**
     * 批量删除考勤记录表
     * @param ids 需要删除的考勤记录表ID
     * @return 结果
     */
    @Transactional(readOnly = false)
    @Override
    public int deleteModAttendanceLogByIds(String[] ids) {
        return mapper.deleteModAttendanceLogByIds(ids, BaseEntity.DEL_FLAG_DELETE);
    }

    @Override
    @Transactional(readOnly = false)
    public Boolean saveBash(String[] ids) {
        List<ModAttendanceLog> users =  new ArrayList<>();
        Arrays.stream(ids).forEach(item -> {
            ModAttendanceLog m = new ModAttendanceLog();
            m.setId(UUID.fastUUID().toString());
            m.setUserId(item);
            m.setRemark("创建缺勤记录");
            m.setCreateBy(SecurityUtils.getUserId());
            m.setCreateTime(new Date());
            m.setVersion(0);
            m.setDelFlag("0");
            users.add(m);
        });
        return mapper.saveBash(users)>0;
    }

}
