package com.aidex.modules.service;

import com.aidex.common.core.domain.TreeNode;
import com.aidex.common.core.domain.entity.SysDept;
import com.aidex.common.core.service.BaseService;
import com.aidex.common.exception.BizException;
import com.aidex.common.utils.NumberUtils;
import com.aidex.modules.domain.ModDormBuild;
import com.aidex.system.common.SysErrorCode;
import com.alibaba.fastjson.JSONObject;
import org.apache.ibatis.annotations.Param;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;

/**
 * dormService接口
 * @author anewhen
 * @email anewhen@foxmail.com
 * @date 2022-06-24
 */
public interface ModDormBuildService extends BaseService<ModDormBuild> {

    /**
     * 批量删除dorm
     * @param ids 需要删除的dormID集合
     * @return 结果
     */
    public int deleteModDormBuildByIds(String[] ids);

    /**
     * 校验名称是否唯一
     *
     * @param build 部门信息
     * @return 结果
     */
    public void checkBuildNameUnique(ModDormBuild build);

    /**
     * 是否存在子节点
     *
     * @param buildId 建筑ID
     * @return 结果
     */
    public boolean hasChildByDeptId(String buildId);

    /**
     * 根据展开层级和父节点递归获取展示的数据
     *
     * @param level
     * @param parentId
     * @return
     */
    public List<ModDormBuild> listDataByLevel(int level, String parentId);

    /**
     * 构建前端所需要树结构
     *
     * @param level    展开层级
     * @param parentId 父节点ID
     * @return
     */
    public List<TreeNode> buildDormTree(int level, String parentId);

    /**
     * 构建前端所需要树结构
     *
     * @param level     展开层级
     * @param parentId  父节点ID
     * @param excludeId 排除节点ID
     * @return
     */
    public List<TreeNode> buildDormTreeExcludeChild(int level, String parentId, String excludeId);

    /**
     * 树检索
     *
     * @param searchText    检索字符
     * @return
     */
    public List<TreeNode> search(String searchText);

    /**
     * 根据部门id串查询部门列表
     * @param dormIdsObj
     * @return
     */
    public List<Map<String,Object>> getDormInfoByIds(JSONObject dormIdsObj);

    /**
     * 查询最大编号
     * @param domain
     * @return
     */
    public Integer findMaxSort(ModDormBuild build);

}
