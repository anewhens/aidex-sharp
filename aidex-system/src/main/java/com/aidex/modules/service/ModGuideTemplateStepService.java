package com.aidex.modules.service;

import com.aidex.common.core.service.BaseService;
import com.aidex.modules.domain.ModGuideTemplateStep;

import java.util.List;

/**
 * 引导模板步骤Service接口
 * @author anewhen
 * @email anewhen@foxmail.com
 * @date 2022-05-17
 */
public interface ModGuideTemplateStepService extends BaseService<ModGuideTemplateStep> {

    /**
     * 批量删除引导模板步骤
     * @param ids 需要删除的引导模板步骤ID集合
     * @return 结果
     */
    public int deleteModGuideTemplateStepByIds(String[] ids);

    /**
     * 根据模板ID查询排序后的步骤列表
     * @param id
     * @return
     */
    public List<ModGuideTemplateStep> queryStepsByTemplateId(String id);

}
