package com.aidex.modules.service;

import com.aidex.common.core.domain.R;
import com.aidex.common.core.service.BaseService;
import com.aidex.modules.domain.ModAttendanceLog;

/**
 * 考勤记录表Service接口
 * @author anewhen
 * @email anewhen@foxmail.com
 * @date 2022-07-26
 */
public interface ModAttendanceLogService extends BaseService<ModAttendanceLog> {

    /**
     * 批量删除考勤记录表
     * @param ids 需要删除的考勤记录表ID集合
     * @return 结果
     */
    public int deleteModAttendanceLogByIds(String[] ids);

    /**
     * 批量提交缺勤记录
     * @param ids 学生ID
     * @return
     */
    public Boolean saveBash(String[] ids);

}
