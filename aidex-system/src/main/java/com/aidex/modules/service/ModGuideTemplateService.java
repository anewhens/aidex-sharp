package com.aidex.modules.service;

import com.aidex.common.core.service.BaseService;
import com.aidex.modules.domain.ModGuideTemplate;

/**
 * 引导模板管理Service接口
 * @author anewhen@foxmail.com
 * @email anewhen@foxmail.com
 * @date 2022-05-16
 */
public interface ModGuideTemplateService extends BaseService<ModGuideTemplate> {

    /**
     * 更新引导模板管理状态
     * @param modGuideTemplate
     * @return 结果
     */
    public int updateStatus(ModGuideTemplate modGuideTemplate);

    /**
     * 批量删除引导模板管理
     * @param ids 需要删除的引导模板管理ID集合
     * @return 结果
     */
    public int deleteModGuideTemplateByIds(String[] ids);

}
