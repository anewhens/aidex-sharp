package com.aidex.modules.service.impl;

import java.util.List;

import com.aidex.modules.domain.ModGuideTemplate;
import com.aidex.modules.mapper.ModGuideTemplateMapper;
import com.aidex.modules.service.ModGuideTemplateService;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.github.pagehelper.PageInfo;
import com.aidex.common.core.domain.BaseEntity;
import com.aidex.common.core.service.BaseServiceImpl;
import org.springframework.transaction.annotation.Transactional;

/**
 * 引导模板管理Service业务层处理
 * @author anewhen@foxmail.com
 * @email anewhen@foxmail.com
 * @date 2022-05-16
 */
@Service
@Transactional(readOnly = true)
public class ModGuideTemplateServiceImpl extends BaseServiceImpl<ModGuideTemplateMapper, ModGuideTemplate> implements ModGuideTemplateService {

    private static final Logger log = LoggerFactory.getLogger(ModGuideTemplateServiceImpl.class);

    /**
     * 获取单条数据
     * @param modGuideTemplate 引导模板管理
     * @return 引导模板管理
     */
    @Override
    public ModGuideTemplate get(ModGuideTemplate modGuideTemplate) {
        ModGuideTemplate dto = super.get(modGuideTemplate);
        return dto;
    }

    /**
     * 获取单条数据
     * @param id 引导模板管理id
     * @return 引导模板管理
     */
    @Override
    public ModGuideTemplate get(String id) {
        ModGuideTemplate dto = super.get(id);
        return dto;
    }

    /**
     * 查询引导模板管理列表
     * @param modGuideTemplate 引导模板管理
     * @return 引导模板管理
     */
    @Override
    public List<ModGuideTemplate> findList(ModGuideTemplate modGuideTemplate) {
		List<ModGuideTemplate> modGuideTemplateList = super.findList(modGuideTemplate);
        return modGuideTemplateList;
    }

    /**
     * 分页查询引导模板管理列表
     * @param modGuideTemplate 引导模板管理
     * @return 引导模板管理
     */
    @Override
    public PageInfo<ModGuideTemplate> findPage(ModGuideTemplate modGuideTemplate) {
		PageInfo<ModGuideTemplate> page = super.findPage(modGuideTemplate);
        return page;
    }

    /**
     * 保存引导模板管理
     * @param modGuideTemplate
     * @return 结果
     */
    @Override
    public boolean save(ModGuideTemplate modGuideTemplate) {
        return super.save(modGuideTemplate);
    }

    /**
     * 更新引导模板管理状态
     * @param modGuideTemplate
     * @return 结果
     */
    @Transactional(readOnly = false)
    @Override
    public int updateStatus(ModGuideTemplate modGuideTemplate) {
        return mapper.updateStatus(modGuideTemplate);
    }

    /**
     * 删除引导模板管理信息
     * @param modGuideTemplate
     * @return 结果
     */
    @Override
    public boolean remove(ModGuideTemplate modGuideTemplate) {
        return super.remove(modGuideTemplate);
    }

    /**
     * 批量删除引导模板管理
     * @param ids 需要删除的引导模板管理ID
     * @return 结果
     */
    @Transactional(readOnly = false)
    @Override
    public int deleteModGuideTemplateByIds(String[] ids) {
        return mapper.deleteModGuideTemplateByIds(ids, BaseEntity.DEL_FLAG_DELETE);
    }

}
