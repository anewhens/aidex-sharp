package com.aidex.modules.service;

import com.aidex.common.core.service.BaseService;
import com.aidex.modules.domain.ModGuideInfo;
import com.aidex.modules.domain.vo.ModGuideTemplateListUserVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 引导模块用户记录Service接口
 * @author anewhen
 * @email anewhen@foxmail.com
 * @date 2022-05-23
 */
public interface ModGuideInfoService extends BaseService<ModGuideInfo> {

    /**
     * 批量删除引导模块用户记录
     * @param ids 需要删除的引导模块用户记录ID集合
     * @return 结果
     */
    public int deleteModGuideInfoByIds(String[] ids);

    /**
     * 批量插入
     * @param list
     * @return
     */
    public int insertBash(List<ModGuideInfo> list);

    /**
     * 根据模板id批量删除进度
     * @param templateId
     */
    public void deleteBashByTemplateId(String templateId);

    /**
     * 根据模板ID用户ID查询引导信息
     * @param templateid
     * @param userId
     * @return
     */
    public ModGuideInfo queryGuideInfoByTemplateIdAndUserId(String templateid, String userId);

    /**
     * 根据登录用户获取模板列表
     * @param userId
     * @return
     */
    public List<ModGuideTemplateListUserVo> queryTemplateListByUserId(String userId);

    /**
     * 更新模板引导信息
     * @param modGuideInfo
     * @return
     */
    public int update(ModGuideInfo modGuideInfo);

}
