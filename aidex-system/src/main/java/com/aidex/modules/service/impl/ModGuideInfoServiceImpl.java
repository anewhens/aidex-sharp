package com.aidex.modules.service.impl;

import java.util.List;

import com.aidex.modules.domain.vo.ModGuideTemplateListUserVo;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.github.pagehelper.PageInfo;
import com.aidex.common.core.domain.BaseEntity;
import com.aidex.common.core.service.BaseServiceImpl;
import com.aidex.modules.mapper.ModGuideInfoMapper;
import com.aidex.modules.domain.ModGuideInfo;
import com.aidex.modules.service.ModGuideInfoService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 引导模块用户记录Service业务层处理
 * @author anewhen
 * @email anewhen@foxmail.com
 * @date 2022-05-23
 */
@Service
@Transactional(readOnly = true)
public class ModGuideInfoServiceImpl extends BaseServiceImpl<ModGuideInfoMapper, ModGuideInfo> implements ModGuideInfoService {

    private static final Logger log = LoggerFactory.getLogger(ModGuideInfoServiceImpl.class);

    /**
     * 获取单条数据
     * @param modGuideInfo 引导模块用户记录
     * @return 引导模块用户记录
     */
    @Override
    public ModGuideInfo get(ModGuideInfo modGuideInfo) {
        ModGuideInfo dto = super.get(modGuideInfo);
        return dto;
    }

    /**
     * 获取单条数据
     * @param id 引导模块用户记录id
     * @return 引导模块用户记录
     */
    @Override
    public ModGuideInfo get(String id) {
        ModGuideInfo dto = super.get(id);
        return dto;
    }

    /**
     * 查询引导模块用户记录列表
     * @param modGuideInfo 引导模块用户记录
     * @return 引导模块用户记录
     */
    @Override
    public List<ModGuideInfo> findList(ModGuideInfo modGuideInfo) {
		List<ModGuideInfo> modGuideInfoList = super.findList(modGuideInfo);
        return modGuideInfoList;
    }

    /**
     * 分页查询引导模块用户记录列表
     * @param modGuideInfo 引导模块用户记录
     * @return 引导模块用户记录
     */
    @Override
    public PageInfo<ModGuideInfo> findPage(ModGuideInfo modGuideInfo) {
		PageInfo<ModGuideInfo> page = super.findPage(modGuideInfo);
        return page;
    }

    /**
     * 保存引导模块用户记录
     * @param modGuideInfo
     * @return 结果
     */
    @Override
    public boolean save(ModGuideInfo modGuideInfo) {
        return super.save(modGuideInfo);
    }

    /**
     * 删除引导模块用户记录信息
     * @param modGuideInfo
     * @return 结果
     */
    @Override
    public boolean remove(ModGuideInfo modGuideInfo) {
        return super.remove(modGuideInfo);
    }

    /**
     * 批量删除引导模块用户记录
     * @param ids 需要删除的引导模块用户记录ID
     * @return 结果
     */
    @Transactional(readOnly = false)
    @Override
    public int deleteModGuideInfoByIds(String[] ids) {
        return mapper.deleteModGuideInfoByIds(ids, BaseEntity.DEL_FLAG_DELETE);
    }

    @Override
    @Transactional(readOnly = false)
    public int insertBash(List<ModGuideInfo> list) {
        return mapper.insertBash(list);
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteBashByTemplateId(String templateId) {
        mapper.deleteBashByTemplateId(templateId);
    }

    @Override
    public ModGuideInfo queryGuideInfoByTemplateIdAndUserId(String templateid, String userId) {
        return mapper.queryGuideInfoByTemplateIdAndUserId(templateid,userId);
    }

    @Override
    public List<ModGuideTemplateListUserVo> queryTemplateListByUserId(String userId) {
        return mapper.queryTemplateListByUserId(userId);
    }

    @Override
    @Transactional(readOnly = false)
    public int update(ModGuideInfo modGuideInfo) {
        return mapper.update(modGuideInfo);
    }

}
