package com.aidex.modules.service.impl;

import java.util.List;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.github.pagehelper.PageInfo;
import com.aidex.common.core.domain.BaseEntity;
import com.aidex.common.core.service.BaseServiceImpl;
import com.aidex.modules.mapper.ModGuideTemplateStepMapper;
import com.aidex.modules.domain.ModGuideTemplateStep;
import com.aidex.modules.service.ModGuideTemplateStepService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 引导模板步骤Service业务层处理
 * @author anewhen
 * @email anewhen@foxmail.com
 * @date 2022-05-17
 */
@Service
@Transactional(readOnly = true)
public class ModGuideTemplateStepServiceImpl extends BaseServiceImpl<ModGuideTemplateStepMapper, ModGuideTemplateStep> implements ModGuideTemplateStepService {

    private static final Logger log = LoggerFactory.getLogger(ModGuideTemplateStepServiceImpl.class);

    /**
     * 获取单条数据
     * @param modGuideTemplateStep 引导模板步骤
     * @return 引导模板步骤
     */
    @Override
    public ModGuideTemplateStep get(ModGuideTemplateStep modGuideTemplateStep) {
        ModGuideTemplateStep dto = super.get(modGuideTemplateStep);
        return dto;
    }

    /**
     * 获取单条数据
     * @param id 引导模板步骤id
     * @return 引导模板步骤
     */
    @Override
    public ModGuideTemplateStep get(String id) {
        ModGuideTemplateStep dto = super.get(id);
        return dto;
    }

    /**
     * 查询引导模板步骤列表
     * @param modGuideTemplateStep 引导模板步骤
     * @return 引导模板步骤
     */
    @Override
    public List<ModGuideTemplateStep> findList(ModGuideTemplateStep modGuideTemplateStep) {
		List<ModGuideTemplateStep> modGuideTemplateStepList = super.findList(modGuideTemplateStep);
        return modGuideTemplateStepList;
    }

    /**
     * 分页查询引导模板步骤列表
     * @param modGuideTemplateStep 引导模板步骤
     * @return 引导模板步骤
     */
    @Override
    public PageInfo<ModGuideTemplateStep> findPage(ModGuideTemplateStep modGuideTemplateStep) {
		PageInfo<ModGuideTemplateStep> page = super.findPage(modGuideTemplateStep);
        return page;
    }

    /**
     * 保存引导模板步骤
     * @param modGuideTemplateStep
     * @return 结果
     */
    @Override
    public boolean save(ModGuideTemplateStep modGuideTemplateStep) {
        return super.save(modGuideTemplateStep);
    }

    /**
     * 删除引导模板步骤信息
     * @param modGuideTemplateStep
     * @return 结果
     */
    @Override
    public boolean remove(ModGuideTemplateStep modGuideTemplateStep) {
        return super.remove(modGuideTemplateStep);
    }

    /**
     * 批量删除引导模板步骤
     * @param ids 需要删除的引导模板步骤ID
     * @return 结果
     */
    @Transactional(readOnly = false)
    @Override
    public int deleteModGuideTemplateStepByIds(String[] ids) {
        return mapper.deleteModGuideTemplateStepByIds(ids, BaseEntity.DEL_FLAG_DELETE);
    }

    /**
     * 根据模板ID获取步骤清单
     * @param id
     * @return
     */
    @Override
    public List<ModGuideTemplateStep> queryStepsByTemplateId(String id) {
        return mapper.queryByTemplateId(id);
    }

}
