package com.aidex.modules.service.impl;

import java.util.List;
import java.util.Map;

import com.aidex.modules.domain.ModDormBuild;
import com.aidex.modules.mapper.ModDormBuildMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.github.pagehelper.PageInfo;
import com.aidex.common.core.domain.BaseEntity;
import com.aidex.common.core.service.BaseServiceImpl;
import com.aidex.modules.mapper.ModDormUserMapper;
import com.aidex.modules.domain.ModDormUser;
import com.aidex.modules.service.ModDormUserService;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 宿舍关联人员表Service业务层处理
 * @author anewhen
 * @email anewhen@foxmail.com
 * @date 2022-07-18
 */
@Service
@Transactional(readOnly = true)
public class ModDormUserServiceImpl extends BaseServiceImpl<ModDormUserMapper, ModDormUser> implements ModDormUserService {

    private static final Logger log = LoggerFactory.getLogger(ModDormUserServiceImpl.class);

    @Resource
    private ModDormBuildMapper modDormBuildMapper;

    /**
     * 获取单条数据
     * @param modDormUser 宿舍关联人员表
     * @return 宿舍关联人员表
     */
    @Override
    public ModDormUser get(ModDormUser modDormUser) {
        ModDormUser dto = super.get(modDormUser);
        return dto;
    }

    /**
     * 获取单条数据
     * @param id 宿舍关联人员表id
     * @return 宿舍关联人员表
     */
    @Override
    public ModDormUser get(String id) {
        ModDormUser dto = super.get(id);
        return dto;
    }

    /**
     * 查询宿舍关联人员表列表
     * @param modDormUser 宿舍关联人员表
     * @return 宿舍关联人员表
     */
    @Override
    public List<ModDormUser> findList(ModDormUser modDormUser) {
		List<ModDormUser> modDormUserList = super.findList(modDormUser);
        return modDormUserList;
    }

    /**
     * 分页查询宿舍关联人员表列表
     * @param modDormUser 宿舍关联人员表
     * @return 宿舍关联人员表
     */
    @Override
    public PageInfo<ModDormUser> findPage(ModDormUser modDormUser) {
		PageInfo<ModDormUser> page = super.findPage(modDormUser);
        return page;
    }

    /**
     * 保存宿舍关联人员表
     * @param modDormUser
     * @return 结果
     */
    @Override
    public boolean save(ModDormUser modDormUser) {
        return super.save(modDormUser);
    }

    /**
     * 删除宿舍关联人员表信息
     * @param modDormUser
     * @return 结果
     */
    @Override
    public boolean remove(ModDormUser modDormUser) {
        return super.remove(modDormUser);
    }

    /**
     * 批量删除宿舍关联人员表
     * @param ids 需要删除的宿舍关联人员表ID
     * @return 结果
     */
    @Transactional(readOnly = false)
    @Override
    public int deleteModDormUserByIds(String[] ids) {
        return mapper.deleteModDormUserByIds(ids, BaseEntity.DEL_FLAG_DELETE);
    }

    @Override
    public int update(ModDormUser modDormUser) {
        return mapper.update(modDormUser);
    }

    @Override
    @Transactional(readOnly = false)
    public String distribution(String buildId, String userId) {
        //校验是否已分配宿舍且在使用
        ModDormUser modDormUser = new ModDormUser();
        modDormUser.setUserId(userId);
        modDormUser.setStatus("0");
        List<ModDormUser> list = this.findList(modDormUser);
        if(list.size()!=0)
            return "已分配，如需重新分配请先操作回退";
        //锁定房间号，同时只允许有一个线程操作同一个房间
        synchronized (buildId){
            //房间人数扣减
            ModDormBuild build = modDormBuildMapper.get(buildId);
            if(build.getCurrentMembers()>=build.getMembers()){
                return "当前房间分配已满，请重新分配，本次分配失败，";
            }
            build.setCurrentMembers(build.getCurrentMembers()+1);
            modDormBuildMapper.update(build);
            modDormUser.setBuildId(buildId);
            save(modDormUser);
        }
        return "分配成功";
    }

    @Override
    public List<Map> selectDistributionLog(String userId) {
        return mapper.selectDistributionLog(userId);
    }

    @Override
    public void dormBack(String userId) {
        ModDormUser modDormUser = new ModDormUser();
        modDormUser.setUserId(userId);
        modDormUser.setStatus("0");
        List<ModDormUser> list = findList(modDormUser);
        if(list.size()!=0){
            ModDormUser updateEntity = list.get(0);
            updateEntity.setStatus("1");
            save(updateEntity);
        }
        ModDormBuild build = modDormBuildMapper.get(list.get(0).getBuildId());
        if(build.getCurrentMembers()-1>=0){
            build.setCurrentMembers(build.getCurrentMembers()-1);
            modDormBuildMapper.update(build);
        }

    }

}
