package com.aidex.modules.mapper;

import com.aidex.common.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.aidex.modules.domain.ModDormUser;

import java.util.List;
import java.util.Map;

/**
 * 宿舍关联人员表Mapper接口
 * @author anewhen
 * @email anewhen@foxmail.com
 * @date 2022-07-18
 */
public interface ModDormUserMapper extends BaseMapper<ModDormUser>
{

    /**
     * 批量删除宿舍关联人员表
     * @param ids 需要删除的宿舍关联人员表ID集合
     * @return
     */
    public int deleteModDormUserByIds(@Param("ids") String[] ids, @Param("DEL_FLAG_DELETE") String DEL_FLAG_DELETE);

    /**
     * 根据用户ID查询分配记录
     * @param userId
     * @return
     */
    public List<Map> selectDistributionLog(@Param("userId")String userId);

}
