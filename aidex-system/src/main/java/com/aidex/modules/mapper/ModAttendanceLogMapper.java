package com.aidex.modules.mapper;

import com.aidex.common.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.aidex.modules.domain.ModAttendanceLog;

import java.util.List;

/**
 * 考勤记录表Mapper接口
 * @author anewhen
 * @email anewhen@foxmail.com
 * @date 2022-07-26
 */
public interface ModAttendanceLogMapper extends BaseMapper<ModAttendanceLog>
{

    /**
     * 批量删除考勤记录表
     * @param ids 需要删除的考勤记录表ID集合
     * @return
     */
    public int deleteModAttendanceLogByIds(@Param("ids") String[] ids, @Param("DEL_FLAG_DELETE") String DEL_FLAG_DELETE);

    /**
     * 批量插入缺勤记录
     * @param users 用户组
     * @return
     */
    public int saveBash(@Param("users") List<ModAttendanceLog> users);

}
