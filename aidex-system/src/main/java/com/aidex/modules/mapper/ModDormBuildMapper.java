package com.aidex.modules.mapper;

import com.aidex.common.core.domain.entity.SysDept;
import com.aidex.common.core.mapper.BaseMapper;
import com.aidex.common.core.mapper.BaseTreeMapper;
import org.apache.ibatis.annotations.Param;
import com.aidex.modules.domain.ModDormBuild;

import java.util.List;
import java.util.Map;

/**
 * dormMapper接口
 * @author anewhen
 * @email anewhen@foxmail.com
 * @date 2022-06-24
 */
public interface ModDormBuildMapper extends BaseTreeMapper<ModDormBuild>
{

    /**
     * 批量删除dorm
     * @param ids 需要删除的dormID集合
     * @return
     */
    public int deleteModDormBuildByIds(@Param("ids") String[] ids, @Param("DEL_FLAG_DELETE") String DEL_FLAG_DELETE);

    /**
     * 根据ID查询所有子建筑（正常状态）
     *
     * @return 子建筑数
     */
    public Integer selectNormalChildByParentIds(String parentIds);


    /**
     * 获取所有下级节点中特定类型的集合
     * @param builds
     * @return
     */
    public List<ModDormBuild> searchChildrenTypeByParentId(ModDormBuild builds);

    /**
     * 修改子元素关系
     *
     * @param builds 子元素
     * @return 结果
     */
    public Integer updateDormChildren(@Param("builds") List<ModDormBuild> builds);

    /**
     * 唯一性校验判断
     *
     * @return 结果
     */
    public Integer findCount(ModDormBuild build);

    public List<ModDormBuild> searchDormTree(ModDormBuild dormBuild);

    /**
     * 根据ids获取所有的节点
     * @param idsList ids
     * @return List<ModDormBuild>
     */
    List<ModDormBuild> searchDormTreeByIds(@Param("ids")List<List<String>> idsList);

    /**
     * 根据id集合获取宿舍对象
     * @param idsList
     * @return
     */
    public List<Map<String,Object>> getDormInfoByIds(@Param("ids")List<List<String>> idsList);

    /**
     * 查询最大编号
     * @param
     * @return int
     */
    public Integer findMaxSort(ModDormBuild build);

}
