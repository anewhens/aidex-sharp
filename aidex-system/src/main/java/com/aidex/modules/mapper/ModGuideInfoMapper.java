package com.aidex.modules.mapper;

import com.aidex.common.core.mapper.BaseMapper;
import com.aidex.modules.domain.ModGuideTemplate;
import com.aidex.modules.domain.vo.ModGuideTemplateListUserVo;
import org.apache.ibatis.annotations.Param;
import com.aidex.modules.domain.ModGuideInfo;

import java.util.List;

/**
 * 引导模块用户记录Mapper接口
 * @author anewhen
 * @email anewhen@foxmail.com
 * @date 2022-05-23
 */
public interface ModGuideInfoMapper extends BaseMapper<ModGuideInfo>
{

    /**
     * 批量删除引导模块用户记录
     * @param ids 需要删除的引导模块用户记录ID集合
     * @return
     */
    public int deleteModGuideInfoByIds(@Param("ids") String[] ids, @Param("DEL_FLAG_DELETE") String DEL_FLAG_DELETE);


    /**
     * 批量插入
     * @param list
     * @return
     */
    public int insertBash(@Param("list") List<ModGuideInfo> list);

    /**
     * 根据模板id批量删除进度
     * @param templateId
     */
    public void deleteBashByTemplateId(@Param("templateId")String templateId);

    /**
     * 根据模板ID用户ID查询引导信息
     * @param templateid
     * @param userId
     * @return
     */
    public ModGuideInfo queryGuideInfoByTemplateIdAndUserId(@Param("templateId")String templateid,@Param("userId")String userId);

    /**
     * 根据登录用户获取模板列表
     * @param userId
     * @return
     */
    public List<ModGuideTemplateListUserVo> queryTemplateListByUserId(@Param("userId")String userId);
}
