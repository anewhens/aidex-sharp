package com.aidex.modules.mapper;

import com.aidex.common.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.aidex.modules.domain.ModGuideTemplateStep;

import java.util.List;

/**
 * 引导模板步骤Mapper接口
 * @author anewhen
 * @email anewhen@foxmail.com
 * @date 2022-05-17
 */
public interface ModGuideTemplateStepMapper extends BaseMapper<ModGuideTemplateStep>
{

    /**
     * 批量删除引导模板步骤
     * @param ids 需要删除的引导模板步骤ID集合
     * @return
     */
    public int deleteModGuideTemplateStepByIds(@Param("ids") String[] ids, @Param("DEL_FLAG_DELETE") String DEL_FLAG_DELETE);

    /**
     * 根据模板ID查询步骤清单
     * @param id
     * @return
     */
    public List<ModGuideTemplateStep> queryByTemplateId(@Param("id")String id);
}
