package com.aidex.modules.mapper;

import com.aidex.common.core.mapper.BaseMapper;
import com.aidex.modules.domain.ModGuideTemplate;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 引导模板管理Mapper接口
 * @author anewhen@foxmail.com
 * @email anewhen@foxmail.com
 * @date 2022-05-16
 */
public interface ModGuideTemplateMapper extends BaseMapper<ModGuideTemplate>
{
    /**
     * 更新引导模板管理状态
     * @param modGuideTemplate 引导模板管理
     * @return 结果
     */
    public int updateStatus(ModGuideTemplate modGuideTemplate);

    /**
     * 批量删除引导模板管理
     * @param ids 需要删除的引导模板管理ID集合
     * @return
     */
    public int deleteModGuideTemplateByIds(@Param("ids") String[] ids, @Param("DEL_FLAG_DELETE") String DEL_FLAG_DELETE);

}
