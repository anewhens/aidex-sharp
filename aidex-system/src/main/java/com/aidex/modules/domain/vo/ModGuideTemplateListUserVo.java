package com.aidex.modules.domain.vo;

import lombok.Data;

import java.util.Date;

/**
 * 根据用户信息列出用户名下所有模板状态正常的模板列表，按升序排列
 */
@Data
public class ModGuideTemplateListUserVo {

    //模板ID
    private String templateId;
    //模板名称
    private String templateName;
    //创建时间
    private String createTime;
    //模板状态
    private String status;
    //步骤
    private Integer step;
    //模板备注
    private String remark;


}
