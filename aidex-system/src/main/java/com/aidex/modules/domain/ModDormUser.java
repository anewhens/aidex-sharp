package com.aidex.modules.domain;

import lombok.Data;
import com.aidex.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.aidex.common.utils.log.annotation.FieldRemark;
import com.aidex.common.utils.log.annotation.LogField;
import com.aidex.common.annotation.Excel;

/**
 * 宿舍关联人员表对象 mod_dorm_user
 * @author anewhen
 * @email anewhen@foxmail.com
 * @date 2022-07-18
 */
@Data
public class ModDormUser extends BaseEntity<ModDormUser>
{
    private static final long serialVersionUID = 1L;

    /** 建筑ID */
    @Excel(name = "建筑ID")
    @LogField
    @FieldRemark(name = "建筑ID",field = "buildId")
    private String buildId;

    /** 关联用户 */
    @Excel(name = "关联用户")
    @LogField
    @FieldRemark(name = "关联用户",field = "userId")
    private String userId;

    /** 0管理 1普通 */
    @Excel(name = "0管理 1普通")
    @LogField
    @FieldRemark(name = "0管理 1普通",field = "charge")
    private String charge;

    /** 在用状态0正常1失效 */
    @Excel(name = "在用状态0正常1失效", dictType = "sys_normal_disable")
    @LogField
    @FieldRemark(name = "在用状态0正常1失效",field = "status")
    private String status;

    public void setBuildId(String buildId) 
    {
        this.buildId = buildId;
    }

    public String getBuildId() 
    {
        return buildId;
    }

    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }

    public void setCharge(String charge) 
    {
        this.charge = charge;
    }

    public String getCharge() 
    {
        return charge;
    }

    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("buildId", getBuildId())
            .append("userId", getUserId())
            .append("charge", getCharge())
            .append("status", getStatus())
            .append("remark", getRemark())
            .append("id", getId())
            .append("createBy", getCreateBy())
            .append("createDept", getCreateDept())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("updateIp", getUpdateIp())
            .append("version", getVersion())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
