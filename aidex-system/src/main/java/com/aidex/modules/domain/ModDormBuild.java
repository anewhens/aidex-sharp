package com.aidex.modules.domain;

import com.aidex.common.core.domain.BaseTreeEntity;
import lombok.Data;
import com.aidex.common.core.domain.BaseEntity;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.aidex.common.utils.log.annotation.FieldRemark;
import com.aidex.common.utils.log.annotation.LogField;
import com.aidex.common.annotation.Excel;

/**
 * dorm对象 mod_dorm_build
 * @author anewhen
 * @email anewhen@foxmail.com
 * @date 2022-06-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ModDormBuild extends BaseTreeEntity<ModDormBuild>
{
    private static final long serialVersionUID = 1L;

    /** 建筑编号 */
    @Excel(name = "建筑编号")
    @LogField
    @FieldRemark(name = "建筑编号",field = "buildCode")
    private String buildCode;

    /** 建筑名称 */
    @Excel(name = "建筑名称")
    @LogField
    @FieldRemark(name = "建筑名称",field = "buildName")
    private String buildName;

    /** 管理员 */
    @Excel(name = "管理员")
    @LogField
    @FieldRemark(name = "管理员",field = "leader")
    private String leader;

    /** 联系电话 */
    @Excel(name = "联系电话")
    @LogField
    @FieldRemark(name = "联系电话",field = "phone")
    private String phone;

    /** 建筑类型 */
    @Excel(name = "建筑类型", dictType = "mod_dorm_build_type")
    @LogField
    @FieldRemark(name = "建筑类型",field = "buildType")
    private String buildType;

    /** 地址 */
    @Excel(name = "地址")
    @LogField
    @FieldRemark(name = "地址",field = "address")
    private String address;

    /** 建筑拼音 */
    @Excel(name = "建筑拼音")
    @LogField
    @FieldRemark(name = "建筑拼音",field = "buildPinyin")
    private String buildPinyin;

    /** 建筑状态（0正常1停用） */
    @LogField
    @FieldRemark(name = "建筑状态",field = "status")
    private String status;

    /** 父级ID */
    private String parentId;

    /** 建筑ID集合 */
    private String parentIds;

    /**
     * 检索字符串，支持名称，编码，拼音
     */
    private String searchText;

    /**
     * 容纳人数
     */
    private Integer members;

    private Integer currentMembers;

    public Integer getMembers() {
        return members;
    }

    public void setMembers(Integer members) {
        this.members = members;
    }

    public Integer getCurrentMembers() {
        return currentMembers;
    }

    public void setCurrentMembers(Integer currentMembers) {
        this.currentMembers = currentMembers;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public void setBuildCode(String buildCode)
    {
        this.buildCode = buildCode;
    }

    public String getBuildCode() 
    {
        return buildCode;
    }

    public void setBuildName(String buildName) 
    {
        this.buildName = buildName;
    }

    public String getBuildName() 
    {
        return buildName;
    }

    public void setLeader(String leader) 
    {
        this.leader = leader;
    }

    public String getLeader() 
    {
        return leader;
    }

    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }

    public void setBuildType(String buildType) 
    {
        this.buildType = buildType;
    }

    public String getBuildType() 
    {
        return buildType;
    }

    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }

    public String getBuildPinyin() {
        return buildPinyin;
    }

    public void setBuildPinyin(String buildPinyin) {
        this.buildPinyin = buildPinyin;
    }


    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    public void setParentId(String parentId) 
    {
        this.parentId = parentId;
    }

    public String getParentId() 
    {
        return parentId;
    }

    public void setParentIds(String parentIds) 
    {
        this.parentIds = parentIds;
    }

    public String getParentIds() 
    {
        return parentIds;
    }

}
