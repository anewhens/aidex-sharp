package com.aidex.modules.domain;

import lombok.Data;
import com.aidex.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.aidex.common.utils.log.annotation.FieldRemark;
import com.aidex.common.utils.log.annotation.LogField;
import com.aidex.common.annotation.Excel;

/**
 * 引导模块用户记录对象 mod_guide_info
 * @author anewhen
 * @email anewhen@foxmail.com
 * @date 2022-05-23
 */
@Data
public class ModGuideInfo extends BaseEntity<ModGuideInfo>
{
    private static final long serialVersionUID = 1L;

    /** 用户ID */
    private String userId;

    /** 模板ID */
    private String templateId;

    /** 当前执行步骤 */
    @Excel(name = "当前执行步骤")
    @LogField
    @FieldRemark(name = "当前执行步骤",field = "step")
    private Long step;

    /** 状态0未完成 1已完成 */
    @Excel(name = "状态0未完成 1已完成", dictType = "sys_normal_disable")
    @LogField
    @FieldRemark(name = "状态0未完成 1已完成",field = "status")
    private String status;

    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }

    public void setTemplateId(String templateId) 
    {
        this.templateId = templateId;
    }

    public String getTemplateId() 
    {
        return templateId;
    }

    public void setStep(Long step) 
    {
        this.step = step;
    }

    public Long getStep() 
    {
        return step;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("userId", getUserId())
            .append("templateId", getTemplateId())
            .append("step", getStep())
            .append("status", getStatus())
            .append("remark", getRemark())
            .append("id", getId())
            .append("createBy", getCreateBy())
            .append("createDept", getCreateDept())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("updateIp", getUpdateIp())
            .append("version", getVersion())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
