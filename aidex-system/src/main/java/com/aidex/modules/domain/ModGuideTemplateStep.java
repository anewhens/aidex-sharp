package com.aidex.modules.domain;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import lombok.Data;
import com.aidex.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.aidex.common.utils.log.annotation.FieldRemark;
import com.aidex.common.utils.log.annotation.LogField;
import com.aidex.common.annotation.Excel;

/**
 * 引导模板步骤对象 mod_guide_template_step
 * @author anewhen
 * @email anewhen@foxmail.com
 * @date 2022-05-17
 */
@Data
public class ModGuideTemplateStep extends BaseEntity<ModGuideTemplateStep>
{
    private static final long serialVersionUID = 1L;

    /** 步骤名称 */
    @Excel(name = "步骤名称")
    @NotBlank(message = "步骤名称不允许为空")
    @LogField
    @FieldRemark(name = "步骤名称",field = "stepName")
    private String stepName;

    /** 主模板ID */
    @Excel(name = "主模板ID")
    @LogField
    @FieldRemark(name = "主模板ID",field = "templateId")
    private String templateId;

    /** 排序 */
    @Excel(name = "排序")
    @LogField
    @FieldRemark(name = "排序",field = "stepSort")
    private Long stepSort;

    /** 步骤类型1人工值守2主动扫码3点击跳过 */
    @Excel(name = "步骤类型1人工值守2主动扫码3点击跳过", dictType = "mod_guide_template_step_type")
    @LogField
    @FieldRemark(name = "步骤类型1人工值守2主动扫码3点击跳过",field = "stepType")
    private String stepType;

    /** 主动扫码二维码 */
    @Excel(name = "主动扫码二维码")
    @LogField
    @FieldRemark(name = "主动扫码二维码",field = "qrCode")
    private String qrCode;

    /** 步骤描述 */
    @Excel(name = "步骤描述")
    @NotBlank(message = "步骤描述不允许为空")
    @LogField
    @FieldRemark(name = "步骤描述",field = "stepDescription")
    private String stepDescription;

    public String getStepName() {
        return stepName;
    }

    public void setStepName(String stepName) {
        this.stepName = stepName;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public Long getStepSort() {
        return stepSort;
    }

    public void setStepSort(Long stepSort) {
        this.stepSort = stepSort;
    }

    public String getStepType() {
        return stepType;
    }

    public void setStepType(String stepType) {
        this.stepType = stepType;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getStepDescription() {
        return stepDescription;
    }

    public void setStepDescription(String stepDescription) {
        this.stepDescription = stepDescription;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("stepName", getStepName())
            .append("stepSort", getStepSort())
            .append("stepType", getStepType())
            .append("qrCode", getQrCode())
            .append("stepDescription", getStepDescription())
            .append("remark", getRemark())
            .append("id", getId())
            .append("createBy", getCreateBy())
            .append("createDept", getCreateDept())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("updateIp", getUpdateIp())
            .append("version", getVersion())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
