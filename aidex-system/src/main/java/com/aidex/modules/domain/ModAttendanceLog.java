package com.aidex.modules.domain;

import lombok.Data;
import com.aidex.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.aidex.common.utils.log.annotation.FieldRemark;
import com.aidex.common.utils.log.annotation.LogField;
import com.aidex.common.annotation.Excel;

/**
 * 考勤记录表对象 mod_attendance_log
 * @author anewhen
 * @email anewhen@foxmail.com
 * @date 2022-07-26
 */
@Data
public class ModAttendanceLog extends BaseEntity<ModAttendanceLog>
{
    private static final long serialVersionUID = 1L;

    private String userId;

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getUserId()
    {
        return userId;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("userId", getUserId())
            .append("remark", getRemark())
            .append("id", getId())
            .append("createBy", getCreateBy())
            .append("createDept", getCreateDept())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("updateIp", getUpdateIp())
            .append("version", getVersion())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
