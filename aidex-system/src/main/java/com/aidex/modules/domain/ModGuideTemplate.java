package com.aidex.modules.domain;

import javax.validation.constraints.NotBlank;
import lombok.Data;
import com.aidex.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.aidex.common.utils.log.annotation.FieldRemark;
import com.aidex.common.utils.log.annotation.LogField;
import com.aidex.common.annotation.Excel;

/**
 * 引导模板管理对象 mod_guide_template
 * @author anewhen@foxmail.com
 * @email anewhen@foxmail.com
 * @date 2022-05-16
 */
@Data
public class ModGuideTemplate extends BaseEntity<ModGuideTemplate>
{
    private static final long serialVersionUID = 1L;

    /** 模板名称 */
    @Excel(name = "模板名称")
    @NotBlank(message = "模板名称不允许为空")
    @LogField
    @FieldRemark(name = "模板名称",field = "templateName")
    private String templateName;

    /** 模板状态 0正在使用 1禁用 */
    @Excel(name = "模板状态 0正在使用 1禁用", dictType = "sys_normal_disable")
    @LogField
    @FieldRemark(name = "模板状态 0正在使用 1禁用",field = "templateStatus")
    private String templateStatus;

    public void setTemplateName(String templateName) 
    {
        this.templateName = templateName;
    }

    public String getTemplateName() 
    {
        return templateName;
    }

    public void setTemplateStatus(String templateStatus)
    {
        this.templateStatus = templateStatus;
    }

    public String getTemplateStatus()
    {
        return templateStatus;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("templateName", getTemplateName())
            .append("templateStatus", getTemplateStatus())
            .append("remark", getRemark())
            .append("id", getId())
            .append("createBy", getCreateBy())
            .append("createDept", getCreateDept())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("updateIp", getUpdateIp())
            .append("version", getVersion())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
